////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

package com.oh24h.rest_for_react_framework;

import com.oh24h.DemoApplication;

public class RestError {
    private int isError = 1;
    private String msg;
    private String place;

    public RestError(String type, Exception e) {
        StringBuilder bf = new StringBuilder(type);
        this.msg = e.getMessage();
        String packageName = DemoApplication.class.getPackage().getName();
        for (StackTraceElement trace:e.getStackTrace()){
            if (trace.getClassName().startsWith(packageName)){
                bf.append(";  ").append(trace.toString().replace(packageName,""));
            }
        }
        this.place = bf.toString();

    }

    public String getPlace() {
        return place;
    }

    public int getIsError() {
        return isError;
    }

    public String getMsg() {
        return msg;
    }
}
