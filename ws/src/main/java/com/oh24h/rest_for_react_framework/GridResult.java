////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

package com.oh24h.rest_for_react_framework;

import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;

public class GridResult {
    private List<Column> cols;
    private List<Object[]> before;
    private List<Object[]> current;
    private List<Object[]> after;
    private List<Object[]> first;
    private List<Object[]> last;
    private int type;

    private List<Object[]> addRowsFromQuery(Session session, String hql, String[] fields, Object[] params, int limit) {

        Query query = session.createQuery(hql).setMaxResults(limit);
        for (int i = 0; i < params.length; i++) {
            query.setParameter("_p" + i, params[i]);
        }
        ScrollableResults scroll = query.scroll();

        if (cols == null) {
            int i = 0;
            cols = new ArrayList<>();
            List<Integer> entityCols = new ArrayList<>();
            for (String fieldName : fields) {

                Class jType = scroll.getType(i).getReturnedClass();
                if (EntityCommon.isEntity(jType)) entityCols.add(i);
                cols.add(new GridResult.Column(fieldName, Metadata.getJavaScriptType(jType, null)));
                i++;
            }
            if (!entityCols.isEmpty())
                throw new RuntimeException("entity can't be used as column. Use entity.id as data, entity.name as desc");
        }


        List<Object[]> result = new ArrayList<>();

        while (scroll.next()) {
            Object[] row = scroll.get();
            for (int i = 0; i < fields.length; i++) {
                if (fields[i].endsWith(".")) row[i] = EntityCommon.restRef(row[i]);
            }
            result.add(row);
        }
        return result;
    }

    private Object[] getParams(Session session, String hql, int currentId) {

        if (currentId <= 0) return null;

        ScrollableResults scroll = session
                .createQuery(hql)
                .setParameter("id", currentId)
                .setMaxResults(1)
                .scroll();

        if (scroll.next()) return scroll.get();
        return null;
    }

    public GridResult(Session session,

                      String fields,
                      String source,
                      int currentId,//0 for first, -1 for last
                      String orderBy,
                      String where,
                      int limit,
                      int type //0:columns,Id, firstT, lastT, predCurrentAfterT;     1:after; -1:before
    ) {
        this.type = type;

        String[] fieldArr = fields.split("\\s*,\\s*");
        String sFields = Arrays.stream(fieldArr)
                .map(field -> field + (field.endsWith(".") ? "ref" : ""))
                .collect(Collectors.joining(", "));


        String fieldID = fieldArr[0];

        where = String.format("%s<>0 ", fieldID) + (where.isEmpty() ? "" : " and " + where);


        String[] orderByArr = orderBy.isEmpty() ? (new String[]{fieldID}) : (orderBy + "," + fieldID).split("\\s*,\\s*");

        Formatter fC = new Formatter();//Current

        Formatter orderA = new Formatter();//f1 asc, f2 asc, f3 desc
        Formatter orderB = new Formatter();//f1 desc, f2 desc, f3 asc

        Formatter whereA = new Formatter();//(f1>:_p1) or (f2>:_p2 and f1=:_p1) or (f3<:_p3 and f2=:_p2 and f1=:_p1) ...
        Formatter whereB = new Formatter();//(f1<:_p1) or (f2<:_p2 and f1=:_p1) or (f3>:_p3 and f2=:_p2 and f1=:_p1)...
        Formatter whereC = new Formatter();//(f1=:_p1 and f2=_p2 and f3=_p3 ...)


        int index = 0;
        for (String s : orderByArr) {
            boolean desc = s.substring(0, 1).equals("-");
            String orderField = s.substring(desc ? 1 : 0);

            if (index == 0) {
                fC.format("Select %s", orderField);

                orderA.format("%s %s", orderField, desc ? " desc" : " asc");
                orderB.format("%s %s", orderField, desc ? " asc" : " desc");

                whereA.format("(%s%s:_p%s)", orderField, desc ? "<" : ">", index);
                whereB.format("(%s%s:_p%s)", orderField, desc ? ">" : "<", index);
                whereC.format("%s=:_p%s", orderField, index);
            } else {
                fC.format(",%s", orderField);

                orderA.format(", %s %s", orderField, desc ? " desc" : " asc");
                orderB.format(", %s %s", orderField, desc ? " asc" : " desc");

                whereA.format("or(%s and %s%s:_p%s)", whereC, orderField, desc ? "<" : ">", index);
                whereB.format("or(%s and %s%s:_p%s)", whereC, orderField, desc ? ">" : "<", index);
                whereC.format(" and %s=:_p%s", orderField, index);
            }

            index++;
        }

        String ParamHQL = fC.format(" from %s where %s=:id", source, fieldID).toString();

        ScrollableResults scroll;

        Object[] params;

        params = getParams(session, ParamHQL, currentId);


        String hqlAfter = String //
                .format("Select %s from %s where %s and (%s) order by %s", sFields, source, where, whereA, orderA);
        String hqlBefore = String
                .format("Select %s from %s where %s and (%s) order by %s", sFields, source, where, whereB, orderB);
        String hqlCurrent = String
                .format("Select %s from %s where %s and %s=%s", sFields, source, where, fieldID, currentId);

        if (params != null)
            current = addRowsFromQuery(session, hqlCurrent, fieldArr, new Array[0], limit);

        if (params != null && (type == 0 || type == 1))
            after = addRowsFromQuery(session, hqlAfter, fieldArr, params, limit);

        if (params != null && (type == 0 || type == -1))
            before = addRowsFromQuery(session, hqlBefore, fieldArr, params, limit);


        if (type == 0) {

            if (before == null) {
                String hqlFirst = String
                        .format("Select %s from %s where %s order by %s", sFields, source, where, orderA);
                first = addRowsFromQuery(session, hqlFirst, fieldArr, new Array[0], limit);
            } else if (before.size() < limit)
                first = new ArrayList<>();
            else {
                String hqlFirst = String
                        .format("Select %s from %s where %s and (%s) order by %s", sFields, source, where, whereB, orderA);
                params = getParams(session, ParamHQL, (int) before.get(before.size() - 1)[0]);
                if (params == null)
                    throw new RuntimeException("some record was deleted in different session");
                first = addRowsFromQuery(session, hqlFirst, fieldArr, params, limit);
            }

            List<Object[]> lastTmp = after != null ? after : first;
            if (lastTmp.size() < limit)
                last = new ArrayList<>();
            else {
                params = getParams(session, ParamHQL, (int) lastTmp.get(lastTmp.size() - 1)[0]);
                if (params == null)
                    throw new RuntimeException("some record was deleted in different session");
                String hqlLast = String
                        .format("Select %s from %s where %s and (%s) order  by %s", sFields, source, where, whereA, orderB);
                last = addRowsFromQuery(session, hqlLast, fieldArr, params, limit);

            }


        }


    }

    public Map<String, Object> getBody() {
        Map<String, Object> result = new LinkedHashMap<>();
        if (type == 0) result.put("cols", cols);
        if (first != null) result.put("first", first);
        if (before != null) result.put("before", before);//orderBy desc
        if (current != null) result.put("current", current);
        if (after != null) result.put("after", after);
        if (last != null) result.put("last", last);//orderBy desc

        return result;
    }


    public static class Column {
        private String data;
        private String type;

        public String getData() {
            return data;
        }

        public String getType() {
            return type;
        }

        Column(String data, String type) {
            this.data = data;
            this.type = type;
        }
    }

}
