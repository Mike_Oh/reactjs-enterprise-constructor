////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

package com.oh24h.rest_for_react_framework;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManagerFactory;
import javax.persistence.metamodel.EntityType;
import java.util.HashMap;
import java.util.LinkedHashMap;


@RestController

//@CrossOrigin(origins = "http://localhost:8080")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class MController {


    @Autowired
    private EntityManagerFactory emf;

    @Autowired
    private Metadata md;

    @GetMapping(path = "metadata")
    Object metadata() {
        try (Session session = emf.unwrap(SessionFactory.class).openSession()) {
            return md.getMetadata(session);
        } catch (RuntimeException e) {
            return new RestError("MController.metadata", e);
        }

    }

    @GetMapping(path = "metadata/{tableName}")
    Object metadataT(@PathVariable String tableName) {


        try {
            EntityType<?> result = md.getTableModel(tableName);
            if (result == null)
                return "table not found:" + tableName;
            else
                return new MdTable(result);

        } catch (RuntimeException e) {
            return new RestError("MController.metadataT", e);
        }
    }

    @GetMapping(path = "grid")
    Object grid(@RequestParam String fields,
                @RequestParam String source,
                @RequestParam(defaultValue = "0") int currentId,//0 for first, -1 for last
                @RequestParam(defaultValue = "") String orderBy,
                @RequestParam(defaultValue = "") String where,
                @RequestParam(defaultValue = "1") int limit,
                @RequestParam(defaultValue = "0") int type //0:columns,Id, firstT, lastT, predCurrentAfterT; // 1:after; -1:before
    ) throws InterruptedException {


        try (Session session = emf.unwrap(SessionFactory.class).openSession()) {
            GridResult gridResult = new GridResult(session, fields, source, currentId, orderBy, where, limit, type);
//            TimeUnit.SECONDS.sleep(1);
            return gridResult.getBody();
        } catch (RuntimeException e) {
            return new RestError("MController.grid", e);
        }


    }

    @PostMapping(path = "record/{table}/{id}")
    Object saveRecord(@RequestBody HashMap<String, Object> body, @PathVariable String table, @PathVariable Integer id) {


        try (Session session = emf.unwrap(SessionFactory.class).openSession()) {

            session.getTransaction().begin();
            ObjectMapper mapper = new ObjectMapper();
            EntityType<?> entity = emf.getMetamodel().getEntities().stream().filter(e -> e.getName().equals(table)).findFirst().get();
            boolean isNew = (id == 0);
            if (isNew) body.remove("id");
            else body.put("id", id);

            Faker.convertMap(body, session);

            Object ob = mapper.convertValue(body, entity.getJavaType());
            if (isNew) id = (Integer) session.save(ob);
            else session.merge(ob);

            session.getTransaction().commit();
            LinkedHashMap<String, Object> result = new LinkedHashMap<>();
            result.put("id", id);
            return result;
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
            return new RestError("MController.grid", e);
        }


    }


    @GetMapping(path = "record/{table}/{id}")
    Object record(@PathVariable String table, @PathVariable Integer id) {

        try (Session session = emf.unwrap(SessionFactory.class).openSession()) {
            EntityResult entityResult = new EntityResult(md, session, table, id);
            return entityResult.getBody();
        } catch (RuntimeException e) {
            return new RestError("MController.grid", e);
        }


    }

    @GetMapping(path = "test")
    String test() {

        try (Session session = emf.unwrap(SessionFactory.class).openSession()) {
            session.getTransaction().begin();
            Query query = session.createQuery("Select id as d, organisation from Orders");
            Object v = query.list();

            ObjectMapper mapper = new ObjectMapper();
            String v2 = mapper.writeValueAsString(v);
            session.getTransaction().commit();
            return v2;
        } catch (RuntimeException | JsonProcessingException e) {
            return e.getMessage();
        }


    }


}
