////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

package com.oh24h.rest_for_react_framework;

import org.hibernate.ScrollableResults;
import org.hibernate.Session;

import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.SingularAttribute;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class EntityResult {

    private Map<String, Object> data = new LinkedHashMap<>();
    private Map<String, Object> types = new LinkedHashMap<>();
    private Map<String, Object> lists;
    private Integer id;


    private Map<String, Object> fillCols(Set<? extends SingularAttribute<?, ?>> attributes, Session session, String tableName, int id) {


        ScrollableResults scroll = session.createQuery(
                String.format("Select %s from %s where id=%s",
                        attributes.stream()
                                .filter(p -> EntityCommon.isDataColumn(p.getName()))
                                .map(p -> p.getName() + (EntityCommon.isEntity(p.getJavaType()) ? ".ref" : ""))
                                .collect(Collectors.joining(","))
                        , tableName, id)
        ).scroll();
        if (!scroll.next()) return data;

        Object[] row = scroll.get();

        attributes.stream()
                .filter(p -> EntityCommon.isDataColumn(p.getName()))
                .forEach(p -> {
                    Object v = row[data.size()];
                    if (EntityCommon.isEntity(p.getJavaType())) v = EntityCommon.restRef(v);
                    data.put(p.getName(), v);
                    types.put(p.getName(), Metadata.getJavaScriptType(p.getJavaType(), p.getJavaMember()));
                });

        return data;
    }

    private Object fillList(Set<? extends SingularAttribute<?, ?>> attributes, Session session, String tableName, int id) {


        Object[] cols = attributes.stream().map(MdTable.MdSingularAttribute::new).toArray();

        String hql = String.format("Select %s from %s where parent=%s",
                attributes.stream()
                        .map(p -> p.getName() + (EntityCommon.isEntity(p.getJavaType()) ? ".ref" : ""))
                        .collect(Collectors.joining(","))
                , tableName, id);
        System.out.println(hql);
        List rows = session.createQuery(hql).list();

        Object[] isEntity = attributes.stream().map(p -> EntityCommon.isEntity(p.getJavaType())).toArray();

        rows.forEach(row -> {
            for (int i = 0; i < isEntity.length; i++) {

                if ((boolean) isEntity[i]) ((Object[]) row)[i] = EntityCommon.restRef(((Object[]) row)[i]);
            }
        });

        Map<String, Object> data = new LinkedHashMap<>();
        data.put("cols", cols);
        data.put("rows", rows);
        return data;
    }

    public EntityResult(Metadata mb, Session session, String tableName, int id) {
        this.id = id;
        EntityType<?> tableModel = mb.getTableModel(tableName);
        if (tableModel == null) return;
        data = fillCols(tableModel.getDeclaredSingularAttributes(), session, tableName, id);
        lists = new LinkedHashMap<>();
        tableModel.getDeclaredPluralAttributes().forEach(attribute -> {
            EntityType<?> tableModelA = (EntityType) attribute.getElementType();
            Object list = fillList(tableModelA.getDeclaredSingularAttributes(), session, tableModelA.getName(), id);
            lists.put(attribute.getName(), list);
        });

    }

    public Map<String, Object> getBody() {
        Map<String, Object> result = new LinkedHashMap<>();
        result.put("attributes", data);
        result.put("lists", lists);
        result.put("types", types);
        result.put("id", id);
        return result;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public Map<String, Object> getLists() {
        return lists;
    }
}
