////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

package com.oh24h.rest_for_react_framework;

import org.hibernate.Session;

import java.util.HashMap;
import java.util.List;

public class Faker {

    public static void convertMap(HashMap<String, Object> body, Session session) {

        for (String key:body.keySet()) {
            Object v = body.get(key);
            if (v instanceof String) v = getRandomID((String) v, session);
            else if (v instanceof List) ((List) v).forEach(row -> convertMap((HashMap<String, Object>) row, session));
            body.put(key,v);

        }
    }

    private static Object getRandomID(String v, Session session) {
        if (v.length()<10) return v;
        if (!v.substring(0, 9).equals("&=random:")) return v;
        String className = v.substring(9);
        int count = ((Long)session.createQuery("select count(*) from "+className)
                .uniqueResult()
        ).intValue();


        int random = 1+(int) (Math.random()*(count-1));

        Integer id = (Integer)session.createQuery("select id from "+className)
                .setFirstResult(random)
                .setMaxResults(1)
                .uniqueResult();

        return id;
    }
}
