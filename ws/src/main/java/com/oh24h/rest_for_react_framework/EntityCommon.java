////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

package com.oh24h.rest_for_react_framework;

public interface EntityCommon {

    static boolean isEntity(Class type) {
        return EntityCommon.class.isAssignableFrom(type);
    }

    static Object[] restRef(Object value) {
        String[] stringArr = ((String) value).split(":", 2);
        return new Object[]{Integer.parseInt(stringArr[0]), stringArr[1]};
    }

    static boolean isDataColumn(String value) {
        return !value.equals("ref") && !value.equals("id");
    }

}
