////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

package com.oh24h.rest_for_react_framework;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.ReplicationMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Time;
import java.util.*;

//import javax.persistence.metamodel.Type;

@Component
public class Metadata implements CommandLineRunner {

    @Autowired
    EntityManagerFactory emf;

    private Metamodel metaModel;

    public static String getJavaScriptType(Class type, Member javaMember) {
        String result = type.getSimpleName();
        String size, integer, fraction;
        if (javaMember == null) {
            size = "?";
            integer = "?";
            fraction = "?";
            //          constKind = "?";
        } else {

//            EntityConst constKindA = (((Field) javaMember).getAnnotation(EntityConst.class));
//            constKind = constKindA==null?"?":constKindA.value();
            Size sizeA = (((Field) javaMember).getAnnotation(Size.class));
            size = sizeA == null || sizeA.max() == 0 ? "255" : "" + sizeA.max();
            Digits digitsA = (((Field) javaMember).getAnnotation(Digits.class));
            integer = digitsA == null || digitsA.integer() == 0 ? "19" : "" + digitsA.integer();
            fraction = digitsA == null || digitsA.fraction() == 0 ? "2" : "" + digitsA.fraction();

        }

        if (type.equals(BigDecimal.class) || type.equals(BigInteger.class)) {
            result = "B:number:" + integer + ":" + fraction;
        } else if (type.equals(Integer.class)) {
            result = "B:REF";//todo
        } else if (type.equals(String.class)) {
            result = "B:text" + ":" + size;
        } else if (type.equals(Boolean.class)) {
            result = "B:boolean";
        } else if (type.equals(java.util.Date.class)) {
            result = "B:datetime";
        } else if (type.equals(Date.class)) {
            result = "B:date";
        } else if (type.equals(Time.class)) {
            result = "B:time";
//        } else if (type == Constants.class) {
//            result = "C:" + constKind;
        } else if (EntityCommon.isEntity(type)) {
            result = "E:" + result;
        } else
            throw new RuntimeException("getJavaScriptType not expected type: " + type);
        return result;
    }

    @Override
    public void run(String... args) throws Exception {

        EntityManager entityManager = emf.createEntityManager();

        metaModel = entityManager.getEntityManagerFactory().getMetamodel();

        try (Session session = emf.unwrap(SessionFactory.class).openSession()) {
            metaModel.getEntities()
                    .stream()
                    .filter(m -> EntityCommon.isEntity(m.getJavaType()))
                    .forEach(tableModel -> {
                        session.getTransaction().begin();
                        Class<?> clazz = tableModel.getJavaType();
                        boolean entityExist = session.get(clazz, 0) != null;

                        HashMap<String, Object> objMap = new HashMap<>();

                        tableModel.getDeclaredSingularAttributes().stream().forEach(a -> {
                            String aName = a.getName();
                            if (!EntityCommon.isDataColumn(aName)) return;
                            Class<?> aClazz = a.getJavaType();
                            Object value = EntityCommon.isEntity(aClazz) ? 0
                                    : aClazz == String.class ? ""
                                    : aClazz == Date.class ? Date.valueOf("0001-01-01")
                                    : aClazz == Time.class ? Time.valueOf("00:00:00")
                                    : aClazz == BigDecimal.class ? 0.0
                                    : aClazz == BigInteger.class ? 0 : null;
                            if (value == null)
                                System.out.println("wrong type in column: " + aName + ", table: " + tableModel.getName());
                            objMap.put(aName, value);
                        });
                        objMap.put("id", 0);

                        try {
                            EntityCommon entity = (EntityCommon) new ObjectMapper().convertValue(objMap, clazz);
                            if (entityExist)
                                session.merge(entity);
                            else
                                session.replicate(entity, ReplicationMode.OVERWRITE);
                            session.getTransaction().commit();
                        } catch (Exception e) {
                            System.out.println("!!! " + tableModel.getName() + "--" + e.getMessage());
                            session.getTransaction().rollback();
                        }

                    });
            System.out.println("---------------");


        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }

    }

    public Metamodel getMetaModel() {
        return metaModel;
    }

//
//    public Object getConstants(Session session) {
//        String hql = "FROM KindsOfConstants where id<>0";
//        List result = session.createQuery(hql).list();
//        return result;
//    }

    public Object getMetadata(Session session) {
        List<MdTable> tables = new ArrayList<>();
        for (EntityType<?> tableModel : metaModel.getEntities()) {
            if (EntityCommon.isEntity(tableModel.getJavaType())) tables.add(new MdTable(tableModel));
        }
        Object constants = "constants";

        Map<String, Object> result = new LinkedHashMap<>();
        //    result.put("constants", getConstants(session));
        result.put("tables", tables);
        return result;
    }


    public EntityType<?> getTableModel(String tableName) {
        for (EntityType<?> tableModel : metaModel.getEntities()) {
            if (tableModel.getName().equals(tableName)) return tableModel;
        }
        return null;
    }
}
