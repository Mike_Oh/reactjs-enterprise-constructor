////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

package com.oh24h.rest_for_react_framework;

import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.PluralAttribute;
import javax.persistence.metamodel.SingularAttribute;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class MdTable {

    private String name;

    private List<MdSingularAttribute> attributes;
    private List<MdPluralAttribute> lists;

    public MdTable(EntityType<?> tableModel) {
        name = tableModel.getName();


        List<Field> f = Arrays.asList(tableModel.getJavaType().getDeclaredFields());
        attributes = tableModel.getDeclaredSingularAttributes().stream()
                .sorted(Comparator.comparingInt(o -> f.indexOf(o.getJavaMember())))
                .map(MdSingularAttribute::new).collect(Collectors.toList());

        lists = tableModel.getDeclaredPluralAttributes().stream()
                .map(MdPluralAttribute::new).collect(Collectors.toList());

    }

    public String getName() {
        return name;
    }

    public List<MdSingularAttribute> getAttributes() {
        return attributes;
    }

    public List<MdPluralAttribute> getLists() {
        return lists;
    }

    public static class MdSingularAttribute {

        private final String data;
        private final String type;

        public String getType() {
            return type;
        }

        public String getData() {
            return data;
        }


        public MdSingularAttribute(SingularAttribute<?, ?> attribute) {
            data = "" + attribute.getName();
            type = Metadata.getJavaScriptType(attribute.getJavaType(), attribute.getJavaMember());
        }
    }

    public static class MdPluralAttribute {

        private final String name;
        private List<MdSingularAttribute> attributes;

        MdPluralAttribute(PluralAttribute<?, ?, ?> attribute) {
            this.name = "" + attribute.getName();
            EntityType<?> tableModel = (EntityType) attribute.getElementType();

            List<Field> f = Arrays.asList(tableModel.getJavaType().getDeclaredFields());
            attributes = tableModel.getDeclaredSingularAttributes().stream()
                .sorted(Comparator.comparingInt(o -> f.indexOf(o.getJavaMember())))
                    .map(MdSingularAttribute::new).collect(Collectors.toList());


        }

        public String getName() {
            return name;
        }

        public List<MdSingularAttribute> getAttributes() {
            return attributes;
        }


    }


}