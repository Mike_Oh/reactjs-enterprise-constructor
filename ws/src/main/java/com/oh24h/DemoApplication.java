////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

package com.oh24h;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {


	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}

