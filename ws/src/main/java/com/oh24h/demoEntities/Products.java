////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

package com.oh24h.demoEntities;

import com.oh24h.rest_for_react_framework.EntityCommon;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;

@Entity
@Data
@NoArgsConstructor
public class Products implements EntityCommon {


    private @Id
    @GeneratedValue
    Integer id;
    public Products(Integer id) {
        this.id = id;
    }

    private @Formula("concat(id, ':', name)")
    String ref;

    private String name;

    private @Digits(integer = 10, fraction = 2)
    BigDecimal price;

    private @ManyToOne(fetch = FetchType.LAZY, optional = false)
    ProductKinds productKind;




}
