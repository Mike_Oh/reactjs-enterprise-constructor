////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

package com.oh24h.demoEntities;

import com.oh24h.rest_for_react_framework.EntityCommon;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Formula;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
public class OrderStatuses implements EntityCommon {

    private @Id
    @GeneratedValue
    Integer id;
    public OrderStatuses(Integer id) {
        this.id = id;
    }


    private @Formula("concat(id, ':', name)")
    String ref;

    private String name;

}
