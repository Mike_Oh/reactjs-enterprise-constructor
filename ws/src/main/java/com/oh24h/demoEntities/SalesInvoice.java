////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

package com.oh24h.demoEntities;

import com.oh24h.rest_for_react_framework.EntityCommon;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class SalesInvoice implements EntityCommon {

    private @Id
    @GeneratedValue
    Integer id;

    public SalesInvoice(Integer id) {
        this.id = id;
    }

    private @Formula("concat(id, ':','Order ', number, ' dated ', DATE_FORMAT(date,'%m/%d/%Y'))")
    String ref;


    private
    String number;

    private
    Date date;

    private
    Time time;

    private @ManyToOne(fetch = FetchType.LAZY, optional = false)
    Organisations organisation;

    private @ManyToOne(fetch = FetchType.LAZY, optional = false)
    Store store;

    private @ManyToOne(fetch = FetchType.LAZY, optional = false)
    Customers customer;

    private @ManyToOne(fetch = FetchType.LAZY, optional = false)
    DeliveryMethods deliveryMethod;

    private
    String comment;

    private @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "parent")
    List<ProductList> productList = new ArrayList<>();


    @Entity
    @Data
    private static class ProductList {

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private Integer id;

        private @ManyToOne(fetch = FetchType.LAZY, optional = false)
        Products product;

        private @Digits(integer = 10, fraction = 2)
        BigDecimal price;

        private @Digits(integer = 10, fraction = 2)
                @Column(name = "count")
        BigDecimal count_r;

        private @Digits(integer = 10, fraction = 2)
        @Column(name = "sum")
        BigDecimal sum_r;


    }

}
