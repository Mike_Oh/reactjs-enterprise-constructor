# ReactJS - Enterprise constructor
The Framework for easy construct large Enterprise applications

demo: http://oh24h.com/ or http://ec2-3-133-101-233.us-east-2.compute.amazonaws.com:8080/ (works with Google Chrome)


The Framework “ReactJS - Enterprise constructor” allows you to quickly deploy UI for information systems with a large number of database tables.

The Framework contains the following features:
1) Full auto-generation of UI forms based on database metadata
2) The ability to manually configure and create forms using React-components that support many properties, auto-configuration and component grouping.
3) Pseudo-grids that provide viewing and navigation on endless data tables (active using of caching)
4) Editing DB-objects (rows) in separate forms (including auto-generated ones)
5) Selection of details from related (by ManyToOne) tables without any additional coding
6) Opening many forms at once in different tabs
7) User-friendly UI
8) Etc

The demo version of the project consists of two modules.
1) React JS
2) Java Spring Boot with restful web service and hibernate demo-entities

The parts of React JS module:
1) AppMetaData.js file
2) demo - forms
3) Framework files

The parts of the Java module
1) Demo Entities classes
2) Framework files

For manual creation of forms, a limited set of components is used (a description of the properties of each component will be added later however using PropTypes allows you to select properties in Intellij / Visual Studio / etc automatically):
1) EForm root component
2) EGroup
3) EField, EButton, ECaption, ECover,
4) EGrid, EGridGroup, EGridField

To create hibernate entities, classes are available
1) regular columns,
2) ManyToOne columns - for store reference to object from another table
3) OneToMany lists, for tabular sections

A source code each form and each java-entity you can see directly from the form.
Press “*” (About)” in the tab on top.
