////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import EForm from "../auto-fetch-rest-data/components/EForm";
import EGrid from "../auto-fetch-rest-data/components/grid/EGrid";
import EGridField from "../auto-fetch-rest-data/components/grid/EGridField";

const H_ProductKinds = () => {

    // function checkWS() {
    //     // this.formDataSource.tt="==>>>";
    //     Api.test().then(r => {
    //         this.formDataSource.tt=r.data;
    //         //console.log("checkWS-ok");
    //         this.updateForm(true, true);
    //     }).catch(r=>{
    //         this.tt='error';
    //         // console.log("checkWS-err")
    //     });
    //     // console.log("checkWS1");
    // };

    function onDoubleClick() {
//        OpenForm('Product', 'id='+this.params.id);
    }


    return <EForm name={'Form'} grouping={"VERTICAL"}>
        {/*<BField/>*/}
        {/*<BGroup grouping={"VERTICAL"}>*/}
        {/*    <BField data={'ff'} captionPosition={'NONE'}/>*/}
        {/*    <EButton text={"check"} onClick={checkWS}/>*/}
        {/*    <BField data={'tt'} captionPosition={'NONE'}/>*/}
            <EGrid dataSource={'ProductKinds'} dataType={"HQL_REST"} maxWidth={4000} onDoubleClick={onDoubleClick} main={"Y"}>
                <EGridField text={'name'} data={'name'} width={250} flexWidth={"Y"} />
            </EGrid>
        {/*</BGroup>*/}
    </EForm>
};

H_ProductKinds.caption = (link) => {
    const {pathname, search} = link;
    if (0) pathname(search);
    return '←Product kinds'
};

export default H_ProductKinds;
