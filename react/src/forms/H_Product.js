////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import EForm from "../auto-fetch-rest-data/components/EForm";
import EField from "../auto-fetch-rest-data/components/form_elements/EField";
import EButton from "../auto-fetch-rest-data/components/form_elements/EButton";
import {MetaContext} from "../auto-fetch-rest-data/common/MainContext";
import EGroup from "../auto-fetch-rest-data/components/form_elements/EGroup";


const H_Product = () => {

    function save() {

        this.save((data)=>window.alert("Saved: "+data.id), (err)=>{MetaContext.showMessage(err);window.alert("Error!")}, true);
    }

    function saveAndClose(p) {

        this.save(()=>this.close(), (err)=>{MetaContext.showMessage(err);window.alert("Error!")});
    }

    function close(p) {
        this.close();
    }



    return <EForm name={'Form'} grouping={"VERTICAL"} dataType={"HQL_REST"}>
        <EGroup grouping={"HORIZONTAL"}>
            <EButton text={"Ok"} onClick={saveAndClose}/>
            <EButton text={"Save"} onClick={save}/>
            <EButton text={"Close"} onClick={close}/>
        </EGroup>
        <EField width={50} flexWidth={"N"} data={'id'} kind={"TEXT"}/>
        <EField data={'name'} width={250} flexWidth={"Y"}/>
        <EField data={'productKind'} width={150} flexWidth={"Y"}/>
    </EForm>
};

export default H_Product;
