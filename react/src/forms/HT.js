////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import EForm from "../auto-fetch-rest-data/components/EForm";
import EGrid from "../auto-fetch-rest-data/components/grid/EGrid";
import EGridField from "../auto-fetch-rest-data/components/grid/EGridField";
import faker from "faker";


const HT = () => {

    const grid1 = {cols:['f1', 'f2', 'f34', 'f3', 'f4', 'f5', 'f6', 'f7'],rows:[]};

    for (let i = 1; i <= 1240; i++) {
        grid1.rows.push(
            [
                i,
                faker.name.findName(),
                faker.internet.email(),
                faker.address.city(),
                faker.address.zipCode(),
                faker.company.companyName(),
                faker.company.bs(),
                faker.finance.amount()
            ]
        );
    }

    const dataSource = {
        lists:{
            grid1:grid1,
        }
    };

    return <EForm name={'Form'} grouping={"HORIZONTAL"} dataSource={dataSource}>
        <EGrid dataSource={'grid1'}>
            <EGridField width={50} flexWidth={"N"} data={'#'}/>
            <EGridField width={400} flexWidth={"N"} data={'f2'}/>
            <EGridField width={350} flexWidth={"N"} data={'f2'}/>
            <EGridField width={300} flexWidth={"N"} data={'f2'}/>
            <EGridField width={610} flexWidth={"N"} data={'f2'}/>
            <EGridField width={410} flexWidth={"N"} data={'f2'}/>
            <EGridField width={110} flexWidth={"Y"} data={'f2'}/>
        </EGrid>

    </EForm>
};

export default HT;
