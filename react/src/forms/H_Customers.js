////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import EForm from "../auto-fetch-rest-data/components/EForm";
import EGrid from "../auto-fetch-rest-data/components/grid/EGrid";
import EGridField from "../auto-fetch-rest-data/components/grid/EGridField";
import EButton from "../auto-fetch-rest-data/components/form_elements/EButton";
import EGroup from "../auto-fetch-rest-data/components/form_elements/EGroup";

const H_Customers = () => {

    function doSelect() {
        this.mainElement.doSelect();
    }

    function doEdit() {
        this.mainElement.doEdit();
    }

    function doNew() {
        this.mainElement.doNew();
    }

    function doDelete() {
        this.mainElement.doDelete();
    }

    const buttons = [];


    buttons.push(<EButton key={1} text={"Edit"} onClick={doEdit}/>);
    buttons.push(<EButton key={2} text={"New"} onClick={doNew}/>);
    buttons.push(<EButton key={4} text={"Delete"} onClick={doDelete}/>);

    return <EForm name={'Form'} grouping={"VERTICAL"}>
        <EGroup grouping={"HORIZONTAL"}>
            {buttons}
        </EGroup>
        <EGrid dataType={"HQL_REST"} dataOrderBy={"name"} maxWidth={4000} main={"Y"}>
            <EGridField data={'name'} width={250} flexWidth={"Y"}/>
            <EGridField data={'address'} width={250} flexWidth={"Y"}/>

        </EGrid>
    </EForm>
};


export default H_Customers;
