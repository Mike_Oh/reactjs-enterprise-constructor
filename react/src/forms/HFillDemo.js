////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import EForm from "../auto-fetch-rest-data/components/EForm";
import EGrid from "../auto-fetch-rest-data/components/grid/EGrid";
import EGridField from "../auto-fetch-rest-data/components/grid/EGridField";
import faker from "faker";
import EButton from "../auto-fetch-rest-data/components/form_elements/EButton";
import {convertForSave, OpenForm} from "../auto-fetch-rest-data/common/commonMetods";
import Api from "../auto-fetch-rest-data/Api";
import {MetaContext} from "../auto-fetch-rest-data/common/MainContext";


const HFillDemo = () => {

    const dataSource = {};

    function onDoubleClick() {
        OpenForm('Customer', 'id=' + this.params.id);
    }


    function addInTable(tName, keyName, source) {
        Api.record(tName, 0).then(r => {
            const newObj = r.data;
            const inUse = [];
            let newId = 0;
            for (let i = 1; i <= 1000; i++) {
                const newV = source[keyName]();
                if (inUse.find(v => v === newV)) continue;
                newId++;
                inUse.push(newV);

                newObj.attributes[keyName] = newV;

                Object.keys(source).filter(key => key !== keyName).forEach(key => newObj.attributes[key] = source[key]());

                Api.save(tName, convertForSave(newObj), -1).then(
                    r => {
                        if (r.data.isError) MetaContext.showMessage("Error: " + r.data.msg + "\n" + r.data.place);
                        else {
                            //MetaContext.showMessage("" + r.data);
                        }
                    }
                ).catch(r => {
                        MetaContext.showMessage(r)
                    }
                )
            }
        })
    }

    function price() {
        return parseFloat(faker.commerce.price());
    }

    function random(obj, types, field) {
        obj[field] = "&=random:" + types[field].split(":")[1];
    }

    function randomInRow(obj, cols, field) {
        //return 0
        obj[field] = "&=random:" + cols.find(col => col.data === field).type.split(":")[1];
    }

    function addOrder(tName) {

        Api.record(tName, 0).then(r => {

            let newId = 0;
            for (let i = 1; i <= 1; i++) {
                const obj0 = r.data;
                const types = obj0.types;
                const newObj = convertForSave(obj0);

                newId++;

                newObj.time = "20:20:20";
                newObj.date = "2019-12-12";
                random(newObj, types, "deliveryMethod");
                random(newObj, types, "store");
                random(newObj, types, "organisation");
                random(newObj, types, "customer");
                newObj.number = faker.random.number({max: 4000, min: 1});
                const rK = faker.random.number({max: 100, min: 5});

                newObj.productList = [];
                for (let i = 1; i <= rK; i++) {
                    const row = {
                        price: 5 * faker.random.number({max: 100, min: 1}),
                        count_r: faker.random.number({max: 100, min: 1}),
                    };
                    randomInRow(row, obj0.lists.productList.cols, "product");
                    row.sum_r = row.price * row.count_r;
                    newObj.productList.push(row)
                }
                Api.save(tName, newObj, 0).then(
                    r => {
                        if (r.data.isError) MetaContext.showMessage("Error: " + r.data.msg + "\n" + r.data.place);
                        else {
                           // MetaContext.showMessage("" + JSON.stringify(r.data));
                        }
                    }
                ).catch(r => {
                        MetaContext.showMessage(r)
                    }
                )
            }
        })
    }

    function add() {
        //addInTable("ProductKinds", 'name',{name:faker.commerce.department})
        //addInTable("Customers", 'name', {name: faker.name.findName, address: faker.address.city})
        // addInTable("Products", 'name', {name: faker.commerce.productName, price: price, productKind:
        //         ()=>faker.random.number({max: 22, min: 0})})
        for (let i = 1; i <= 1000; i++)
            addOrder("Orders")

    }


    return <EForm name={'Form'} grouping={"VERTICAL"} dataSource={dataSource}>
        <EButton text={"generate"} onClick={add}/>
        <EGrid dataType={"HQL_REST"} main={"Y"} dataSource={"Orders"}>
            <EGridField text={'customer'} data={'customer.'} width={250} flexWidth={"Y"}/>

        </EGrid>

    </EForm>
};


export default HFillDemo;
