////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import EForm from "../auto-fetch-rest-data/components/EForm";
import EField from "../auto-fetch-rest-data/components/form_elements/EField";
import EGroup from "../auto-fetch-rest-data/components/form_elements/EGroup";
import EButton from "../auto-fetch-rest-data/components/form_elements/EButton";
import './H8.css'
import ECaption from "../auto-fetch-rest-data/components/form_elements/ECaption";
import EGrid from "../auto-fetch-rest-data/components/grid/EGrid";
import EGridField from "../auto-fetch-rest-data/components/grid/EGridField";
import {MetaContext} from "../auto-fetch-rest-data/common/MainContext";


const H8 = () => {
    const dataSource = {
        attributes: {
            form: 8,
            customer: 'Nimble bug',
            vr: 'www',
            s: 'err', Description: '123', grid1: 'grid1', grid2: 'grid2',
        },
        lists: {
            table: {
                cols: ['name', 'age'],
                rows: [
                    ['Ivan', 22],
                    ['Mike', 41]
                ]
            }
        }

    };

    function onClick() {
        MetaContext.showMessage('onClick');
    }

    function onChange() {
        MetaContext.showMessage('onChange');
    }


    return <EForm name={'Form'} grouping={"VERTICAL"} sameCaptionSizeIn1Column={"Y"} maxWidth={1500}
                  captionMaxWidth={180} dataSource={dataSource}>
        <EGroup grouping={"HORIZONTAL"} className={''}>
            <EGroup grouping={"VERTICAL"} sameCaptionSizeIn1Column={"Y"}>
                <EGroup grouping={"HORIZONTAL"}>
                    <EField caption={'Date'} width={60}/>
                    <EField caption={'Number'} width={120}/>
                </EGroup>
                <EField caption={'Customer'} data={'customer'} onClick={onClick} onChange={onChange}/>
                <EField data={'Description'} caption={'Description'} kind={"TEXTAREA"} flexWidth={"N"} width={200}
                        uiProps={{title: 'Description of customer'}}/>
                <EField caption={'Customer2'} data={'customer'} onClick={onClick}/>
            </EGroup>
            <EGroup grouping={"VERTICAL"} sameCaptionSizeIn1Column={"Y"} className={'Test1'} caption={'Test1'}>
                <EField caption={'Organisation'}/>
                <EField data={'Description'}
                        caption={'Description  Description  Description  Description  Description'}
                        kind={"TEXTAREA"} width={20} flexWidth={"Y"}
                        uiProps={{title: 'Description of organisation'}}/>
                <EField caption={'Tags'}/>
            </EGroup>

        </EGroup>
        <ECaption caption={'it is caption'} text={'Caption'}/>
        <EGroup grouping={"PAGES"} className={''}>
            <EGroup grouping={"VERTICAL"} caption={'Grid'}>
                <EGrid dataSource={"table"}>
                    <EGridField text={"Name"} data={"name"}/>
                    <EGridField text={"age"} data={"age"}/>
                </EGrid>

            </EGroup>
            <EGroup grouping={"VERTICAL"} caption={'Page with advanced data'}>
                <EField caption={'Field 1'}/>
                <EField caption={'Field 2'}/>
                <EField caption={'Field 3'}/>
                <EField caption={'Field 4'}/>
                <EField data={'Field 5'} caption={'Field 5'} kind={"TEXTAREA"} captionPosition={"LEFT"}
                        flexHeight={"Y"}/>
            </EGroup>
            <EGroup grouping={"VERTICAL"} caption={'Pages in page'}>
                <EGroup grouping={"PAGES"} className={''}>
                    <EGroup grouping={"VERTICAL"} caption={'1'}>
                        <EField caption={'field 1'}/>
                    </EGroup>
                    <EGroup grouping={"VERTICAL"} caption={'2'}>
                        <EField caption={'field 2'} kind={"TEXTAREA"} flexWidth={"N"}/>
                    </EGroup>
                    <EGroup grouping={"VERTICAL"} caption={'3'}>
                        <EField width={150} caption={'field 3'} kind={"TEXTAREA"} flexHeight={"Y"}/>
                    </EGroup>
                </EGroup>
            </EGroup>
        </EGroup>

        <EGroup grouping={"HORIZONTAL"}>
            <EField caption={'comment'}/>
            <EField caption={'author'} width={100}/>
        </EGroup>

        <EGroup grouping={"HORIZONTAL"}>
            <EButton text={'Save'}/>
            <EButton text={'Reload'}/>
            <EButton text={'Cancel'}/>
        </EGroup>

    </EForm>
};

export default H8;
