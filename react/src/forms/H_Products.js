////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import EForm from "../auto-fetch-rest-data/components/EForm";
import EGrid from "../auto-fetch-rest-data/components/grid/EGrid";
import EGridField from "../auto-fetch-rest-data/components/grid/EGridField";
import EField from "../auto-fetch-rest-data/components/form_elements/EField";
import EButton from "../auto-fetch-rest-data/components/form_elements/EButton";
import EGroup from "../auto-fetch-rest-data/components/form_elements/EGroup";

const H_Products = () => {


    const formObject = {
        attributes: {productKind: [0, ""], test:"12"},
        types: {productKind: "E:ProductKinds",test:"B:text:255"},
    };

    function onChangeFilter(){
        const grid = this.elements.find(e=>e.name === "grid");
        const newV = formObject.attributes.productKind[0];
        grid.grid.gridContext.dataFilter = newV?"productKind.id="+newV:"";
        grid.grid.gridContext.refresh();
    }


    function doEdit() {
        this.mainElement.doEdit();
    }

    function doNew() {
        this.mainElement.doNew();
    }

    function doDelete() {
        this.mainElement.doDelete();
    }

    const buttons = [];


    buttons.push(<EButton key={1} text={"Edit"} onClick={doEdit}/>);
    buttons.push(<EButton key={2} text={"New"} onClick={doNew}/>);
    buttons.push(<EButton key={4} text={"Delete"} onClick={doDelete}/>);

    return <EForm name={'Form'} grouping={"VERTICAL"} dataSource={formObject}>
        <EGroup grouping={"HORIZONTAL"}>
            {buttons}
            <EField caption={"Kind"} name={"filter"} data={"productKind"} onChange={onChangeFilter}/>
        </EGroup>
        <EGrid name={"grid"} dataType={"HQL_REST"} maxWidth={4000} main={"Y"}>
            <EGridField data={'name'} width={250} flexWidth={"Y"}/>
            <EGridField text={'Kind'} data={'productKind.'} width={250} flexWidth={"Y"}/>
            <EGridField data={'price'} width={50} flexWidth={"Y"}/>

        </EGrid>
        {/*</EGroup>*/}
    </EForm>
};

H_Products.caption = (link) => {
    const {pathname, search} = link;
    if (0) pathname(search);
    return 'Products'
};

export default H_Products;
