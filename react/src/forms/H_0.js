////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import EForm from "../auto-fetch-rest-data/components/EForm";
import EGrid from "../auto-fetch-rest-data/components/grid/EGrid";
import EGridField from "../auto-fetch-rest-data/components/grid/EGridField";
import EGridGroup from "../auto-fetch-rest-data/components/grid/EGridGroup";
import faker from 'faker';
import EGroup from "../auto-fetch-rest-data/components/form_elements/EGroup";


const H_0 = () => {

    const grid1 = {cols: ['f1', 'f2', 'f34', 'f3', 'f4', 'f5', 'f6', 'f7'], rows: []};
    const grid2 = {cols: ['f1', 'f2', 'f3', 'f4', 'f5'], rows: []};
    const grid3 = {cols: ['f1', 'f2', 'f3', 'f4', 'f5'], rows: []};

    for (let i = 1; i <= 1240; i++) {
        const commerce = faker.commerce;
        grid1.rows.push(
            [
                commerce.department(),
                commerce.productName(),
                faker.company.companyName(),
                faker.address.city(),
                faker.address.city(),
                faker.address.zipCode(),
                commerce.price(),

            ]
        );
    }

    for (let i = 1; i <= 2; i++) {
        grid2.rows.push(
            [
                faker.name.findName(),
                faker.address.city(),
                faker.address.zipCode(),
                faker.company.companyName(),
            ]
        );
    }

    for (let i = 1; i <= 20; i++) {
        grid3.rows.push(
            [
                faker.name.findName(),
                faker.address.city(),
                faker.address.zipCode(),
                faker.company.companyName(),
            ]
        );
    }

    const dataSource = {
        attributes: {
        },
        lists: {
            grid1: grid1,
            grid2: grid2,
            grid3: grid3
        }
    };

    return <EForm name={'Form'} grouping={"VERTICAL"} dataSource={dataSource}>
        {/*<BField/>*/}
        <EGroup grouping={"HORIZONTAL"}>
            <EGrid dataSource={'grid2'} maxWidth={400}>
                <EGridField width={50} flexWidth={"N"} data={'#'}/>
                <EGridField data={'f2'} width={250} flexWidth={"N"}/>
                <EGridField data={'f3'} width={250} flexWidth={"N"}/>
                <EGridField data={'f4'} width={250} flexWidth={"N"}/>
                <EGridField data={'f5'} width={250} flexWidth={"N"}/>
            </EGrid>
            <EGrid dataSource={'grid3'} maxWidth={400}>
                <EGridField width={50} flexWidth={"N"} data={'#'}/>
                <EGridField data={'f2'} width={250} flexWidth={"N"}/>
                <EGridField data={'f3'} width={250} flexWidth={"N"}/>
                <EGridField data={'f4'} width={250} flexWidth={"N"}/>
                <EGridField data={'f5'} width={250} flexWidth={"N"}/>
            </EGrid>
            <EGrid caption={'GRID'} captionPosition={"NONE"} dataSource={'grid1'}>
                <EGridField width={50} flexWidth={"N"} data={'#'}/>
                <EGridField data={'f2'} width={350} flexWidth={"N"}/>
                <EGridGroup text={'gr1'} grouping={"VERTICAL"}>
                    <EGridField data={'f34'} width={300} flexWidth={"N"}/>
                    <EGridGroup text={'gr2'} grouping={"HORIZONTAL"}>
                        <EGridField text={'f3'} data={'f3'} width={250} flexWidth={"N"}/>
                        <EGridField text={'f4'} data={'f4'} width={150} flexWidth={"N"}/>
                    </EGridGroup>
                </EGridGroup>
                <EGridGroup text={'gr3'} grouping={"TOGETHER"}>
                    <EGridField text={'f5'} data={'f5'} width={150} flexWidth={"N"}/>
                    <EGridField text={'f6'} data={'f6'} width={250} flexWidth={"N"}/>
                </EGridGroup>
                <EGridField text={'f7'} data={'f7'} width={250}/>
            </EGrid>
        </EGroup>
    </EForm>
};

H_0.caption = (link) => {
    const {pathname, search} = link;
    if (0) pathname(search);
    return 'H_0'
};

export default H_0;
