////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import EForm from "../auto-fetch-rest-data/components/EForm";
import EField from "../auto-fetch-rest-data/components/form_elements/EField";
import EButton from "../auto-fetch-rest-data/components/form_elements/EButton";
import EGroup from "../auto-fetch-rest-data/components/form_elements/EGroup";
import EGrid from "../auto-fetch-rest-data/components/grid/EGrid";
import EGridField from "../auto-fetch-rest-data/components/grid/EGridField";
import {MetaContext} from "../auto-fetch-rest-data/common/MainContext";


const H_Order = () => {

    function save() {

        this.save((data)=>window.alert("Saved: "+data.id), (err)=>{MetaContext.showMessage(err);window.alert("Error!")}, true);
    }

    function saveAndClose(p) {

        this.save(()=>this.close(), (err)=>{MetaContext.showMessage(err);window.alert("Error!")});
    }

    function close(p) {
        this.close();
    }


    return <EForm name={'Form'} grouping={"VERTICAL"} dataType={"HQL_REST"} dataSource={"Orders"}>
        <EGroup grouping={"HORIZONTAL"}>
            <EButton text={"Ok"} onClick={saveAndClose}/>
            <EButton text={"Save"} onClick={save}/>
            <EButton text={"Close"} onClick={close}/>
        </EGroup>
        <EGroup grouping={"HORIZONTAL"}>
            <EField caption={'number'} width={100} flexWidth={"Y"} data={'number'} kind={"INPUT"}/>
            <EField caption={'date'} width={150} flexWidth={"N"} data={'date'} kind={"INPUT"}/>
            <EField caption={'time'} width={100} flexWidth={"N"} data={'time'} kind={"INPUT"}/>
        </EGroup>
        <EGroup grouping={"HORIZONTAL"}>
            <EGroup grouping={"VERTICAL"}>
                <EField caption={'customer'} data={'customer'} width={250} flexWidth={"Y"}/>
                <EField caption={'organisation'} data={'organisation'} width={250} flexWidth={"Y"}/>
            </EGroup>
            <EGroup grouping={"VERTICAL"} sameCaptionSizeIn1Column={"Y"}>
                <EField caption={'store'} data={'store'} width={250} flexWidth={"Y"}/>
                <EField caption={'delivery method'} data={'deliveryMethod'} width={250} flexWidth={"Y"}/>
            </EGroup>
        </EGroup>
        <EGroup grouping={"PAGES"}>

            <EGrid caption={'products'} dataSource={"productList"} height={50} flexHeight={"Y"}>
                <EGridField text={'#'} data={"#"} flexWidth={"N"} width={30}/>
                <EGridField text={'product'} data={"product"} flexWidth={"Y"}/>
                <EGridField text={'price'} data={"price"} width={100} flexWidth={"N"}/>
                <EGridField text={'count'} data={"count_r"} width={100} flexWidth={"N"}/>
                <EGridField text={'sum'} data={"sum_r"} width={100} flexWidth={"N"}/>
            </EGrid>
            <EField caption={'comment'} data={'comment'} height={10} width={250} flexWidth={"Y"} flexHeight={"Y"}
                    kind={"TEXTAREA"}/>
        </EGroup>

    </EForm>
};


export default H_Order;
