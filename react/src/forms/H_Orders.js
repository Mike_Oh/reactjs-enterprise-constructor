////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import EForm from "../auto-fetch-rest-data/components/EForm";
import EGrid from "../auto-fetch-rest-data/components/grid/EGrid";
import EGridField from "../auto-fetch-rest-data/components/grid/EGridField";
import EButton from "../auto-fetch-rest-data/components/form_elements/EButton";
import EGroup from "../auto-fetch-rest-data/components/form_elements/EGroup";
import EField from "../auto-fetch-rest-data/components/form_elements/EField";


const H_Orders = () => {
    function doEdit() {
        this.mainElement.doEdit();
    }

    function doNew() {
        this.mainElement.doNew();
    }

    function doDelete() {
        this.mainElement.doDelete();
    }

    const buttons = [];


    buttons.push(<EButton key={1} text={"Edit"} onClick={doEdit}/>);
    buttons.push(<EButton key={2} text={"New"} onClick={doNew}/>);
    buttons.push(<EButton key={4} text={"Delete"} onClick={doDelete}/>);


    const formObject = {
        attributes: {organisation: [0, ""], test:"12"},
        types: {organisation: "E:Organisations",test:"B:text:255"},
    };

    function onChangeFilter(){
        const grid = this.elements.find(e=>e.name === "grid");
        const newV = formObject.attributes.organisation[0];
        grid.grid.gridContext.dataFilter = newV?"organisation.id="+newV:"";
        grid.grid.gridContext.refresh();
    }


    return <EForm name={'Form'} grouping={"VERTICAL"} dataSource={formObject}>
        <EGroup grouping={"HORIZONTAL"}>
            {buttons}
            <EField name={"filter"} data={"organisation"} onChange={onChangeFilter}/>
        </EGroup>
        <EGrid name={"grid"} dataType={"HQL_REST"} main={"Y"} dataOrderBy={"number"}>
            <EGridField data={'number'} width={150} flexWidth={"N"}/>
            <EGridField data={'customer.'} width={250} flexWidth={"Y"}/>
            <EGridField data={'organisation.'} width={250} flexWidth={"Y"}/>
            <EGridField data={'store.'} width={250} flexWidth={"Y"}/>
            <EGridField data={'store.id'} width={250} flexWidth={"Y"}/>

        </EGrid>
        {/*</EGroup>*/}
    </EForm>
};


export default H_Orders;
