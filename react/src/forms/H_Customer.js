////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import EForm from "../auto-fetch-rest-data/components/EForm";
import EField from "../auto-fetch-rest-data/components/form_elements/EField";

const H_Customer = () => {

    return <EForm name={'Form'} grouping={"VERTICAL"} dataType={"HQL_REST"} dataSource={"Customers"}>
        <EField data={'name'} width={250} flexWidth={"Y"}/>
    </EForm>
};

export default H_Customer;
