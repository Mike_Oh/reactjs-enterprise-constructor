////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import EForm from "../../auto-fetch-rest-data/components/EForm";
import ECaption from "../../auto-fetch-rest-data/components/form_elements/ECaption";


const ErrorForm = () => {

    return <EForm>
        <ECaption text={"404"}/>
    </EForm>
};

export default ErrorForm;
