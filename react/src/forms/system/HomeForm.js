////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import EForm from "../../auto-fetch-rest-data/components/EForm";
import ECover from "../../auto-fetch-rest-data/components/form_elements/ECover";


export const HomeForm = () => {

    return <EForm name={'Form'}>
        <ECover height={1000} width={750}>
            <p><strong>ReactJS - Enterprise constructor</strong></p>
            <p>The Framework for easy construct large Enterprise applications</p>
            <p>demo: http://oh24h.com/ or http://ec2-3-133-101-233.us-east-2.compute.amazonaws.com:8080/
            <br/>(works with Google Chrome)<br/></p>
            <p>The Framework &ldquo;ReactJS - Enterprise constructor&rdquo; allows you to quickly deploy UI for
                information systems with a large number of database tables.</p>
            <p>The Framework contains the following features:</p>
            <ol>
                <li>Full auto-generation of UI forms based on database metadata</li>
                <li>The ability to manually configure and create forms using React-components that support many
                    properties, auto-configuration and component grouping.
                </li>
                <li>Pseudo-grids that provide viewing and navigation on endless data tables (active using of caching)
                </li>
                <li>Editing DB-objects (rows) in separate forms (including auto-generated ones)</li>
                <li>Selection of details from related (by ManyToOne) tables without any additional coding</li>
                <li>Opening many forms at once in different tabs</li>
                <li>User-friendly UI</li>
                <li>Etc</li>
            </ol>
            <p>The demo version of the project consists of two modules.</p>
            <ol>
                <li>React JS</li>
                <li>Java Spring Boot with restful web service and hibernate demo-entities</li>
            </ol>
            <p>The parts of React JS module:</p>
            <ol>
                <li>AppMetaData.js file</li>
                <li>demo - forms</li>
                <li>Framework files</li>
            </ol>
            <p>The parts of the Java module</p>
            <ol>
                <li>Demo Entities classes</li>
                <li>Framework files</li>
            </ol>
            <p>For manual creation of forms, a limited set of components is used (a description of the properties of
                each component will be added later however using PropTypes allows you to select properties in Intellij /
                Visual Studio / etc automatically):</p>
            <ol>
                <li>EForm root component</li>
                <li>EGroup</li>
                <li>EField, EButton, ECaption, ECover,</li>
                <li>EGrid, EGridGroup, EGridField</li>
            </ol>
            <p>To create hibernate entities, classes are available</p>
            <ol>
                <li>regular columns,</li>
                <li>ManyToOne columns - for store reference to object from another table</li>
                <li>OneToMany lists, for tabular sections</li>
            </ol>
            <p>A source code each form and each java-entity you can see directly from the form.</p>
            <p>Press &ldquo;*&rdquo; (About)&rdquo; in the tab on top.</p>
        </ECover>
    </EForm>
};


export default HomeForm;
