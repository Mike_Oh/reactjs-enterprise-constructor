////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react';


const AuthorizationForm = () => <div>Login form here</div>;

AuthorizationForm.needAuthorization = () => false;

export default AuthorizationForm;