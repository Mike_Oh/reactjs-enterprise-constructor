////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import PropTypes from "prop-types";
import {useUi} from "../uiContext";
import ETextUI from "../piece/ETextUI";


const EPagesList = (props) => {

    const {onSelectPage, activePage, titles, name, pagesClassName} = props;

    const ui = useUi(props, EPagesList, true);

    const UiBase = ETextUI;

    const childrenUI = [];
    titles.forEach((titleStr, i) => {

        const uiPropsBase = {
            flexWidth: 'N', uiGroup: ui,
            key: name + '_c' + i, name: name + '_c' + i, text: titleStr.caption,
            className: pagesClassName + '' + (activePage === i ? 'Active' : ''),
            onClick: () => onSelectPage(i),

        };
        childrenUI.push(<UiBase {...uiPropsBase}/>);

    });

    const uiPropsBase = {
        width: 1, flexWidth: 'Y', uiGroup: ui,
        key: name + '_c', name: name + '_c', text: '\xa0',
        className: pagesClassName + 'Space'
    };


    childrenUI.push(<UiBase {...uiPropsBase}/>);

    return [childrenUI];


};
EPagesList.isComponent = true;
EPagesList.isHorizontal = true;


export default EPagesList;

EPagesList.propTypes = {

    name: PropTypes.string,
    text: PropTypes.string,
    caption: PropTypes.string,
    captionPosition: PropTypes.oneOf(['NONE', 'LEFT', 'TOP']),
    flexWidth: PropTypes.oneOf(['Y', 'N']),
    flexHeight: PropTypes.oneOf(['Y', 'N']),
    width: PropTypes.number,
    height: PropTypes.number,
    maxWidth: PropTypes.number,
    maxHeight: PropTypes.number,
    visible: PropTypes.oneOf(['Y', 'N']),
    uiProps: PropTypes.object
};


EPagesList.defaultProps = {
    data: '',
    name: '',
    caption: '',
    captionPosition: 'LEFT',
    flexWidth: 'Y',
    flexHeight: 'N',
    width: 0,//to-do add auto for 0 width
    height: 0, //to-do add auto for 0 height
    maxWidth: 0,
    maxHeight: 0,
    visible: 'Y',
};