////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import PropTypes from 'prop-types'
import React from 'react';
import {useUi} from "../uiContext";
import ECaption from "../form_elements/ECaption";


const EWithCaption = (props) => {
    const {captionPresent, caption, visible} = props;
    const ui = useUi(props, EWithCaption);


    const child = props.children;//only child
    const ChildType = child.type;


    let newChild;


    const {grouping, isPagesBody} = child.props;
    const {children, name, ...otherChildProps} = child.props;
    const childProps = {...otherChildProps, key: name, name: name, uiGroup: ui};

    if (ChildType.isGroup && grouping === 'PAGES' && !isPagesBody) {
        childProps.grouping = 'VERTICAL';
        childProps.isPages = true;
        childProps.margin=true;

    }

    if (!children) newChild = <ChildType {...childProps}/>;
    else newChild = <ChildType {...childProps}>{children}</ChildType>;


    if (captionPresent) {
        const captionProps = {
            key: name + '_caption', name: name + '_caption', uiGroup: ui,
            isCaption: true, visible: visible, text: caption + ':'

        };
        if (ChildType.isGroup) captionProps.margin=true;

        newChild = [<ECaption {...captionProps}/>, newChild];
    }

    return newChild;
};
EWithCaption.isGroup = true;

export default EWithCaption;

EWithCaption.propTypes = {
    caption: PropTypes.any,
    captionPresent: PropTypes.any,
    children: PropTypes.any,
    grouping: PropTypes.any,
    isPagesBody: PropTypes.any,
    name: PropTypes.any,
    visible: PropTypes.any
};