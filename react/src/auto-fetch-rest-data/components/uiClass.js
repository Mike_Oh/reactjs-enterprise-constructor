////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import {MetaContext} from "../common/MainContext";
import {doUserHook, hooksByTypes} from "./uiHooks";
import {fillProps, OpenSelectForm, zeroProps} from "../common/commonMetods";
import {pickToType} from "../common/typeControl";

export class UiClass {

    constructor(props, UiType, form) {
        form.elements.push(this);
        const {grouping, name, isCaption, data} = props;

        this.group = props.uiGroup;
        this.name = name;
        this.form = form;


        if (!!this.group && this.group.gridContext) this.gridContext = this.group.gridContext;

        //fillPresentProps(this, UiType, 'isGroup,isUi,isResizable,flexToGroup,isGrid,isRowUi');
        if (UiType.isGroup) this.isGroup = true;
        if (UiType.isUi) this.isUi = true;
        if (UiType.isUi && props.baseUI) this.baseUI = true;
        if (UiType.isResizable) this.isResizable = true;
        if (UiType.flexToGroup) this.flexToGroup = true;
        if (UiType.isGrid) this.isGrid = true;
        if (UiType.isRowUi) this.isRowUi = true;
        if (UiType.minWidthIgnoringChildren) this.minWidthIgnoringChildren = UiType.minWidthIgnoringChildren;

        //if (this.isUi&&data) this.data = data;
        if (data) this.data = data;

        if (grouping === 'VERTICAL' || UiType.isVertical) this.isVertical = true;
        if (grouping === 'HORIZONTAL' || UiType.isHorizontal) this.isHorizontal = true;

        if (this.isGroup && !!props.className) this.hasDiv = true;
        //if (!!props.className) this.hasDiv = true;

        if (props.margin) this.margin = true;

        if (this.isUi && isCaption) this.isCaption = true;


        if (this.isUi || this.hasDiv) this.ref = React.createRef();

        if (0) this.fMinW = 0;
        if (0) this.hidden = false;
        zeroProps(this, 'h,w,x,y');

        this.updateInitUiSize(props);
        if (this.isUi) this.w = this.minW;
        if (this.isUi) this.h = this.minH;

        if (this.isUi) this.hooks = this.getHooks(props, UiType);

        if (this.isGroup) this.sameCaptionSizeIn1Column = (props.sameCaptionSizeIn1Column === 'Y');

        if (!this.isUi && props.maxWidth) this.gMaxW = props.maxWidth;
        if (!this.isUi && props.maxHeight) this.gMaxH = props.maxHeight;
        //if (this.isGroup) this.gMaxW = props.maxWidth || 0;
        //if (this.isGroup) this.gMaxH = props.maxHeight || 0;

    }


    onHook(hookName, params, notToDoStandard) {

        const userHook = this.hooks && this.hooks.userHooks[hookName];
        const {form, ref, data} = this;
        let doUpdateData = false;
        let doUpdateUI = false;
        if (hookName === 'onChange' && !notToDoStandard) {

            if (this.isGrid) {
                const colIndex = this.gridContext.dataTable.cols.findIndex(col => col.data === params.col);
                if (colIndex < 0) return;
                this.gridContext.dataTable.rows[params.row][colIndex] = params.newValue;
                doUpdateData = true;
            } else {
                const attributes = form.formData.object.attributes;
                const newValue = pickToType(ref.current.value, attributes[data], this.getDataType());
                if (!!data) attributes[data] = newValue;
                doUpdateData = true;

            }


            //this.form.doUpdateData = true;
        }

        if (!!userHook) {
            doUpdateData = true;
            doUserHook(this, params, userHook);
        }

        form.updateForm(doUpdateData, doUpdateUI);


    }

    getHooks(props, UiType) {
        const possibleHooks = hooksByTypes[UiType.name];

        const hooks = {};
        const userHooks = {};

        if (!!possibleHooks)
            possibleHooks.forEach(hookName => {

                const userHook = props[hookName];
                let needHook = !!userHook;

                if (needHook) userHooks[hookName] = userHook;

                needHook = needHook || hookName === 'onChange';
                needHook = needHook || (hookName === 'onClick' && !!this.gridContext);

                if (needHook) hooks[hookName] = () => this.onHook(hookName);


            });
        return {hooks: hooks, userHooks: userHooks};

    }

    updateFp() {
        let fpW, fpH;
        if (this.hasDiv) {
            const current = this.ref.current;
            fpW = current.offsetWidth - current.clientWidth;
            fpH = current.offsetHeight - current.clientHeight;
        } else if (this.isUi) {
            const current = this.ref.current;
            fpW = current.offsetWidth - (this.w);
            fpH = current.offsetHeight - (this.h);
        } else {
            fpW = 0;
            fpH = 0
        }
        const result = (fpW !== this.fpW || fpH !== this.fpH);
        this.fpH = fpH;
        this.fpW = fpW;
        return result;
    }

    updateInitUiSize(props) {
        if (this.isUi) {

            let minW, maxW, minH, maxH;

            minW = props.width || 0;
            maxW = props.maxWidth111 || 0;
            if (minW && maxW && minW > maxW)
                minW = maxW;

            if (props.flexWidth !== 'Y')
                if (minW) maxW = minW; else minW = maxW;
            else if (maxW === 0) maxW = 10000;

            this.minW = minW;
            this.maxW = maxW;

            minH = props.height || 0;
            maxH = props.maxHeight111 || 0;

            if (minH && maxH && minH > maxH)
                minH = maxH;

            if (props.flexHeight !== 'Y')
                if (minH) maxH = minH; else minH = maxH;
            else {
                if (maxH === 0) maxH = 10000;
                if (minH === 0) minH = MetaContext.minHForFlexible;
            }


            this.minH = minH;
            this.maxH = maxH;

        }
    }

    setChildrenSize() {
        let delta;
        const isHorizontal = this.isHorizontal;
        const isVertical = this.isVertical;
        let children;
        const initW = this.w;
        const initH = this.h;

        this.w = Math.max(this.w, this.minW);
        this.w = Math.min(this.w, this.maxW);

        children = this.children.filter(child => child.minW !== child.maxW);

        delta = this.w - (this.inerMinW || this.minW);
        let delta1 = Math.ceil((this.w) / children.length);

        children.sort((a, b) => a.minW - b.minW)
            .forEach((child, i) => {
                const minW = child.minW;
                if (isHorizontal) {

                    if (i === children.length - 1) {
                        child.w = minW + delta;
                    } else {
                        child.w = Math.max(delta1, minW);
                    }
                    child.w = Math.min(child.w, child.maxW);
                    delta -= (child.w - minW);

                } else child.w = Math.min(child.maxW, this.w - child.fpW - (this.margin ? MetaContext.elementMarginW : 0));

            });

        //if (isHorizontal&&delta) children[0].w+=delta;


        this.h = Math.max(this.h, this.minH);
        this.h = Math.min(this.h, this.maxH);
        delta = (this.h - this.minH);

        children = this.children.filter(child => child.minH !== child.maxH);
        delta1 = this.h / children.length;
        children.sort((a, b) => a.minH - b.minH)
            .forEach((child, i) => {
                const minH = child.minH;
                if (isVertical) {
                    if (i === children.length - 1) {
                        child.h = minH + delta;
                    } else {
                        child.h = Math.max(delta1, minH);
                        delta -= (child.h - minH);
                    }

                } else child.h = Math.min(child.maxH, this.h - child.fpH - (this.margin ? MetaContext.elementMarginH : 0));

            });

        this.children.forEach(child => {
            if (child.flexToGroup && !this.isGrid) {
                if (!isHorizontal || child.isUi)
                    fillProps(child, this, 'w,minW,maxW');

                if (!isVertical || child.isUi)
                    fillProps(child, this, 'h,minH,maxH');
                if (child.isUi) {
                    zeroProps(child, 'fpH,fpW')
                }

            }
            if (!child.isUi) child.setChildrenSize();
        });

        // if (!this.group) {
        this.w = Math.max(initW, this.w);
        this.h = Math.max(initH, this.h);
        // }

    }

    setSameCaptionSize() {

        //MetaContext.showMessage('sameCaptionSizeIn1Column=' + ui.name);
        const arrayMarches = [];
        let maxSize = 0;

        const fillArrayMarches = (ui) => {
            for (const child of ui.children) {
                if (child.isCaption) {
                    arrayMarches.push(child.group);
                    maxSize = Math.max(child.fpW, maxSize)

                } else fillArrayMarches(child);
                if (!ui.isVertical) break;
            }
        };
        fillArrayMarches(this);
        arrayMarches.forEach(child => child.fMinW = maxSize)

    };

    setChildrenXY() {
        let x = this.x;
        let y = this.y;

        let isVertical = this.isVertical;
        let isHorizontal = this.isHorizontal;

        this.children.forEach(child => {
            if (this.hasDiv) {
                child.x = x - this.x;
                child.y = y - this.y;
            } else {
                child.x = x;
                child.y = y;
            }

            child.x += (child.margin ? MetaContext.elementMarginW / 2 : 0);
            child.y += (child.margin ? MetaContext.elementMarginH / 2 : 0);


            child.setChildrenXY();
            if (isHorizontal) x += child.w + child.fpW;
            if (isVertical) y += child.h + child.fpH;
        })

    }

    collectMinMax() {


        let tmp, minW = 0, maxW = 0, minH = 0, maxH = 0;
        const isVertical = this.isVertical, isHorizontal = this.isHorizontal, gMaxW = this.gMaxW,
            gMaxH = this.gMaxH;

        this.children.forEach(child => {
            if (!child.isUi) child.collectMinMax();

            tmp = child.minW + (child.fpW || 0);
            minW = isHorizontal ? minW + tmp : Math.max(minW, tmp);

            tmp = child.maxW + (child.fpW || 0);
            maxW = isHorizontal ? maxW + tmp : Math.max(maxW, tmp);

            tmp = child.minH + (child.fpH || 0);
            minH = isVertical ? minH + tmp : Math.max(minH, tmp);

            tmp = child.maxH + (child.fpH || 0);
            maxH = isVertical ? maxH + tmp : Math.max(maxH, tmp);

        });

        minW += (this.margin ? MetaContext.elementMarginW : 0);
        minH += (this.margin ? MetaContext.elementMarginH : 0);

        if (gMaxW) maxW = Math.min(maxW, gMaxW);

        minW = Math.max(minW, this.fMinW || 0);
        maxW = Math.max(minW, maxW);

        if (gMaxH) maxH = Math.min(maxH, gMaxH);
        maxH = Math.max(minH, maxH);

        if (this.minWidthIgnoringChildren) {
            this.inerMinW = minW;
            minW = this.minWidthIgnoringChildren;
        }

        this.minW = minW;
        this.maxW = maxW;
        this.minH = minH;
        this.maxH = maxH;


    }

    divProps(props) {
        const rez = {className: props.className};
        rez.ref = this.ref;
        if (this.w || this.h) rez.style = {
            position: 'absolute',
            left: this.x + 'px',
            top: this.y + 'px',
        };

        if (this.w) rez.style.width = this.w + 'px';
        if (this.h) rez.style.height = this.h + 'px';


        if (this.hidden) rez.hidden = true;
        return {...rez}
    }

    uiProps(props, row) {
        const rez = {...props.uiProps, className: props.className, ...this.hooks.hooks};
        rez.ref = this.ref;
        const kf = !row ? 0 : this.gridContext.rowHeight * row;
        const y = this.yy || this.y + kf;
        rez.style = {
            position: 'absolute',
            left: this.x + 'px',
            top: y + 'px',
        };

        const w = this.ww || this.w;
        const h = this.hh || this.h;

        if (w) rez.style.width = w + 'px';
        if (h) rez.style.height = h + 'px';


        if (!!this.gridContext) {
            rez.onMouseDown = () => {
                this.gridContext.onSelect(row, this.name)
            };
            rez.onDoubleClick = () => {
                this.gridContext.onDoubleClick(this.gridContext.dataTable.rows[this.gridContext.currentRow], this.name)
            };
        } else if (this.isResizable) rez.onMouseDown = () => {
            this.form.elementOnResize = this
        };


        if (this.hidden) rez.hidden = true;
        if (this.isCaption) rez.style.maxWidth = this.form.captionMaxWidth + 'px';
        return rez
    }

    get children() {
        return this.form.elements.filter(child => (child.group === this));
    }

    getDataType() {
        let dataType;
        if (this.isRowUi) {
            const col = this.gridContext.dataTable.cols.find(col => col.data === this.data);
            if (!col) return;
            dataType = col.type;
            const defdfd = 545445;
        } else {
            const types = this.form.formData.object.types;
            if (!types) return;
            dataType = (this.form.formData.object.types[this.data]);
        }

        if (!dataType) return;

        return dataType.split(":");
    }

    onKeyDown(e) {
        if (e.key === "F4" && !e.shiftKey && !e.ctrlKey && !e.altKey && !e.metaKey) {
            const dataType = this.getDataType();
            if (!dataType) return;
            const isEntity = (dataType[0] === "E");
            if (isEntity) OpenSelectForm(this.name);
        } else if (e.key === "F4" && e.shiftKey && !e.ctrlKey && !e.altKey && !e.metaKey) {
            const dataType = this.getDataType();
            if (!dataType) return;
            const isEntity = (dataType[0] === "E");
            this.form.formData.object.attributes[this.data] = isEntity ? [0, ""] : "";
            this.form.updateForm();
        }
    }

    onAddonDo(name) {

        this.form.elements.find(e => e.group === this).ref.current.focus();

        const dataType = this.getDataType();
        if (!dataType) return;
        const isEntity = (dataType[0] === "E");
        if (name === "del") {
            this.form.formData.object.attributes[this.data] = isEntity ? [0, ""] : "";
            this.doHook("onChange");
        }
        if (name === "more" && isEntity) {
            OpenSelectForm(this.name);
        }
        //     this.form.formData.object.attributes[this.data] =isEntity?[0,""]:"";
        //
        //     if (this.form.formData.object.types[this.data])
        // console.log("click on:"+name);
    }

    doHook(hookName) {
        const baseUI = this.form.elements.find(e => e.group === this && e.baseUI);
        if (baseUI) baseUI.onHook(hookName, undefined, true);
        //this.form.doUpdateData = true;
    }

    onChoose(value) {

        if (this.isRowUi){
            const row = this.gridContext.currentRow;
            const params = {row : row, col: this.data, newValue: value};
            this.gridContext.ui.onHook("onChange", params);
        }else {
            const attributes = this.form.formData.object.attributes;
            attributes[this.data] = pickToType(value, attributes[this.data], this.getDataType());
            this.doHook("onChange");
        }



    }

    onFocus() {
        this.form.activeElement = this;
    }

}

