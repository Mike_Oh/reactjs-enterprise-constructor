////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from "react";
import ECover from "./form_elements/ECover";
import {MetaData} from "../common/MainContext";

function recursiveClone(root, names, rootProps) {

    let newName, newChildren;
    const type = root.type;
    if (type.isGroup || type.isComponent || type.isForm || type.isCover) {
        newName = root.props.name.trim();

        if (!newName || !!names.find(used => used === newName))
            for (let i = 1; true; i++) {
                newName = '' + root.type.name + i;
                if (!names.find(used => used === newName)) break;
            }
        names.push(newName);
    } else {
        for (let i = 1; true; i++) {
            newName = 'cover' + i;
            if (!names.find(used => used === newName)) break;
        }
        names.push(newName);
        return <ECover key={newName} name={newName}>{root}</ECover>
        //return <ECoverUI key={newName} name={newName}>root</ECoverUI>
    }

    if (type.isCover)
        newChildren = root.props.children;
    else
        newChildren = React.Children.map(root.props.children, child => recursiveClone(child, names));

    return React.cloneElement(root, {key: newName, name: newName, ...rootProps}, newChildren);
}



const EFormStarter = (props) => {
    const {component, params} = props;
    const names = [];
    const [showFrame, setShowFrame] = React.useState(false);

    React.useEffect(() => {
        setShowFrame(true);
    });

    if (!showFrame) return <></>;

    try {

        return recursiveClone(component ? component() : MetaData.generateForm(params.addProps.entity, params.addProps.kind), names, {params: params})
    } catch (er) {


        return <>
            <div>{er.message}</div>
            {er.stack}</>
    }

};

export default EFormStarter;