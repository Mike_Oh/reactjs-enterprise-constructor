////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import EForm from "./EForm";
import React from "react";
import Api from "../Api";
import {TimeStampToString} from "../common/commonMetods";

export const useDataContext = (props, form) => {

    form.receiveData = () => {
        const {source, id} = form.formData;
        Api.record(source, id).then(r => {
            form.formData.object = r.data;
            form.formData.ready = 1;
            form.updateForm();
        });
    };


    if (!form.formData) {
        //id: params.id,
        //table: 'Users',

        form.formData = {dataType: props.dataType};

        if (props.dataType === 'OBJECT') {
            form.formData.isObject = true;
            form.formData.object = props.dataSource;
            form.formData.ready = 1;
        } else if (props.dataType === 'HQL_REST') {
            form.formData.isRest = true;
            form.formData.source = props.params.addProps.entity.name;
            form.formData.id = props.params.addProps.id;
            form.formData.object = {};
            form.formData.ready = 0;
            form.receiveData();
        }
    }

    if (form.doUpdateData) {
        form.doUpdateData = false;
        form.dataContext++;
    }
    React.useLayoutEffect(() => {
        if (form.formData.isRest)
            form.receiveData();
    }, []);


    //console.log("EForm-dataContext:"+form.dataContext);
};


export const useData = (props) => {
    const {data} = props;
    const form = React.useContext(EForm.context);
    React.useContext(EForm.DataContext);
    const attributes = form.formData.object.attributes;
    const types = form.formData.object.types;
    if (attributes === undefined) return null;
    if (data === undefined) return null;

    let value = attributes[data];
    if (Array.isArray(value)) value = value[1];
    if (!!types && !!types[data] && types[data] === "B:datetime") value = TimeStampToString(value);
    //"2019-11-12T01:01";
    if (value === undefined) value = null;
    return value;
};

export const useGridData = (props, gridContext, gRow) => {

    const row = gRow + gridContext.firstGridRow;
    React.useContext(EForm.context);
    React.useContext(EForm.DataContext);
    let value;

    if (Array.isArray(props.data))
        value = props.data.map(data => gridContext.value(row, data)).toString();
    else value = gridContext.value(row, props.data);

    return value;
};
