////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from "react";
import Api from "../Api";
import {convertForSave} from "../common/commonMetods";
import {MetaContext} from "../common/MainContext";

export const hooksByTypes = {

    Form: ['onAfterDataChanged', 'onBeforeDataChanged'],
    EButton: ['onClick'],
    EInputUI: ['onClick', 'onChange'],
    ETextAreaUI: ['onClick', 'onChange'],
    ETextUI: ['onClick'],
    ECaptionUI: ['onClick'],
    EGridRowUI: ['onClick'],
};

export const setHooks = (form) => {
    const params = form.params;

    form.close = () => {
        params.addProps.close();
    };

    form.mainElement = () => {
        return form.elements.find(e => (!!e.isMain));
    };

    form.getSelectParams = (fieldName) => {
        const element = form.elements.find(el => el.name === fieldName);
        const type = element.getDataType();
        let current;
        if (element.isRowUi) {
            const colIndex = element.gridContext.dataTable.cols.findIndex(col => col.data === element.data);
            current = element.gridContext.dataTable.rows[element.gridContext.currentRow][colIndex];
        } else
            current = form.formData.object.attributes[element.data];
        return {type: type, current: current, onChoose: element.onChoose.bind(element)};
    };

    // form.updateGrids = () => {
    //     const qq=1
    // };

    form.choose = (v) => {
        if (params.addProps.selectParams && params.addProps.selectParams.onChoose) {
            params.addProps.selectParams.onChoose(v);
            form.close();
        }
    };

    form.onKeyDown = (e) => {
        if (e.type !== 'keydown') return;
        if (e.ctrlKey || e.shiftKey || e.altKey) return;
        if (e.key === 'Escape') form.close();
    };

    form.onMouseUp = () => {
        if (form.elementOnResize) {
            if (form.elementOnResize.updateFp()) {
                form.updateForm()
            }
            form.elementOnResize = undefined;
        }
    };
    form.onClickTest = () => {

    };


};

export const setUserHooks = (form) => {

    const userContext = {
        get elements() {
            //todo - need to convert []->{}, filter only userDefine, add methods and props
            return form.elements;
        },
        get mainElement() {
            const main = form.mainElement();
            return main && main.gridContext;
        },
        get formObject() {
            return form.formData.object
        },
        _formContext: form,
        close: form.close,
        updateForm: form.updateForm,
    };
    if (form.kind === "objectForm") userContext.save = (onOk, onErr, replaceNew) =>
        Api.save(
            form.params.addProps.entity.name,
            convertForSave(form.formData.object),
            form.formData.object.id)
            .then(r => {
                if (r.data.isError) onErr("Error: " + r.data.msg + "\n" + r.data.place);
                else {

                    const id = r.data.id;
                    if (form.formData.id === "0" && replaceNew) {
                        //form.formData.object.id = newId;
                        //form.params.addProps.id = "" + newId;
                        form.params.pathname = form.params.pathname.substr(0, form.params.pathname.length - 1) + id;
                        //form.params.addProps.updateTabs();
                        MetaContext.routerHistory.replace(form.params.pathname);
                        form.close();
                    }

                    const dataName = form.formData.source;

                    MetaContext.openForms.forEach(f =>
                        f.elements
                            .filter(el => el.isGrid && el.gridContext.isRest && el.gridContext.dataName === dataName)
                            .forEach(grid => {
                                grid.gridContext.receiveData(0, parseInt(id))
                            })
                    );

                    onOk(r.data);
                }
            })
            .catch(r =>
                onErr("Error: " + r)
            );


    if (form.kind === "form" || form.kind === "chooseForm") {

    }

    form.userContext = userContext;

};

export const doUserHook = (ui, params, userHook) => {
    const {form, ref, data} = ui;


    const userContext = {
        ...form.userContext,
        params: params,
        __userHook_: userHook
    };

    userContext.__userHook_();

    //userContext.updateGrids = this.form.updateGrids;


    // if (userContext.updateViewLayer) form.doUpdateUI = true;
    //if (userContext.updateDataLayer) form.doUpdateData = true;

};