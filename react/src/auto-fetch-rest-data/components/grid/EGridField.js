////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import PropTypes from "prop-types";
import {useUi} from "../uiContext";
import EGridHeaderUI from "./EGridHeaderUI";
import EGridRowUI from "./EGridRowUI";

const EGridField = (props) => {

    const {rowType, name, kind, children, titleHeight, height, text, ...otherProps} = props;
    const ui = useUi(props, EGridField);

    const uiProps = {
        ...otherProps, key: name + '_ui', name: name + '_ui', uiGroup: ui
    };

    if (rowType==='h') uiProps.text = text;
    uiProps.height = rowType==='h' ? titleHeight : height;

    if (rowType==='h') return <EGridHeaderUI {...uiProps}/>;

    return <EGridRowUI {...uiProps}/>


};

EGridField.defSize = {
    minWidth: 50,
};

EGridField.isComponent = true;
EGridField.flexToGroup = true;
export default EGridField


EGridField.propTypes = {

    data: PropTypes.any,
    dataRef: PropTypes.any,
    name: PropTypes.string,
    text: PropTypes.string,
    flexWidth: PropTypes.oneOf(['Y', 'N']),
    width: PropTypes.number,
    maxWidth: PropTypes.number,
    height: PropTypes.number,
    titleHeight: PropTypes.number,
    visible: PropTypes.oneOf(['Y', 'N']),
    uiProps: PropTypes.object,
    onClick: PropTypes.func,
    onChange: PropTypes.func,
};


EGridField.defaultProps = {
    data: '',
    dataRef:'',
    name: '',
    height: 25,
    titleHeight: 30,
    width: 40,
    maxWidth: 10000,
    flexWidth: 'Y',
    // maxHeight: 0,
    visible: 'Y',

};