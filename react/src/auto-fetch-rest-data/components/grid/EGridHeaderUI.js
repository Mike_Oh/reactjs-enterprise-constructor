////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import PropTypes from 'prop-types'
import React from 'react'
import {useUi} from "../uiContext";
import {captionFromData} from "../../common/commonMetods";

const EGridHeaderUI = (props) => {
    const {is0, text, data} = props;
    const ui = useUi(props, EGridHeaderUI);

    const uiProps = ui.uiProps(props);

    const className = is0?'EGridUI0':'EGridHeaderUI';

    const style = (ui.w&&ui.h)
        ?{position: 'absolute', top:0, right:0, bottom:0, left:0}
        :undefined;
    const normalText = captionFromData(text, data);
    return (<div {...uiProps}><div className={className} style={style} title={normalText}>{normalText}</div></div>);


};

EGridHeaderUI.isUi = true;
EGridHeaderUI.flexToGroup = true;

export default EGridHeaderUI;

EGridHeaderUI.propTypes = {
  classNameUi: PropTypes.any,
  name: PropTypes.any,
  text: PropTypes.any
};
