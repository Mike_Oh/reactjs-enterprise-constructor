////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from "react";
import {useUi} from "../uiContext";
import {useData} from "../dataContext";


const EGridVoid = (props) => {

    const ui = useUi(props, EGridVoid, true);
    const className='EGridRowUI';

    useData(props);

    const uiProps = ui.uiProps(props);

    return <div {...uiProps} className={className}/>;

};

EGridVoid.isUi = true;

EGridVoid.propTypes = {

    // data: PropTypes.string,
    // name: PropTypes.string,
    // kind: PropTypes.oneOf(['INPUT', 'TEXTAREA', 'TEXT']).isRequired,
    // caption: PropTypes.string,
    // captionPosition: PropTypes.oneOf(['NONE', 'LEFT', 'TOP']),
    // flexWidth: PropTypes.oneOf(['Y', 'N', 'AUTO']),
    // flexHeight: PropTypes.oneOf(['Y', 'N', 'AUTO']),
    // width: PropTypes.number,
    // height: PropTypes.number,
    // maxWidth: PropTypes.number,
    // maxHeight: PropTypes.number,
    // visible: PropTypes.oneOf(['Y', 'N']),
    // uiProps: PropTypes.object,
    // onClick: PropTypes.func,
    // onChange: PropTypes.func,
};


EGridVoid.defaultProps = {
    flexWidth: 'Y',
    flexHeight: 'Y',
    width: 1,
    height: 1,

};
export default EGridVoid;
