////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import PropTypes from 'prop-types'
import React from 'react'
import {useUi} from "../uiContext";
import {useGridData} from "../dataContext";
import {setTypeRestrictionForInput} from "../../common/typeControl";
import {OpenSelectForm} from "../../common/commonMetods";

const EGridRowUI = (props) => {
    const {row, is0, name} = props;
    const ui = useUi(props, EGridRowUI);
    const dataType = ui.getDataType();

    const gridContext = ui.gridContext;
    const value = useGridData(props, gridContext, row);


    const gridRow = gridContext.gridRow;
    const activeCol = gridContext.activeCol;


    const className = is0 ?
        'EGridUI0' : row === gridRow && name === activeCol ?
            'EGridRowUIColSelected' : row === gridRow ?
                'EGridRowUIRowSelected' : 'EGridRowUI';

    const uiProps = ui.uiProps(props, row);


    //MetaContext.showMessage(name+':'+props.uiGroup.w+'==='+ui.w);

    const editMode = dataType && gridContext.editMode && row === gridRow && name === activeCol;

    //todo refactoring source code
    if (editMode) {
        ui.form.activeElement = undefined;
        const isSelect = dataType[0] === "E";
        const onChange = (v) => {
            if (isSelect) return;
            const params = {row : row, col: ui.data, newValue: v.target.value};

            gridContext.ui.onHook("onChange", params);

        };

        const onAddonDo = (name) =>{

            const isEntity = (dataType[0] === "E");

            if (name === "del") {
                const params = {row : row, col: ui.data, newValue: isEntity ? [0, ""] : ""};
                gridContext.ui.onHook("onChange", params);
            }
            if (name === "more" && isEntity) {
                OpenSelectForm(ui.name);
            }
        };

        console.log("111");
        const addons = isSelect ? 2 : 1;
        //gridContext.editedRef = React.createRef();

        const inputProps = {
            value: value,
            onChange: onChange,
          //  ref: gridContext.editedRef;
            style: {position: 'absolute', width: "100%", top: 0, bottom: 0}
        };
        setTypeRestrictionForInput(inputProps, dataType);

        const childrenUI = [<div key="input"
                                 style={{position: 'absolute', top: 0, right: (addons * 19) + 4, bottom: 0, left: 0}}>
            <input {...inputProps}/>
        </div>];




        if (isSelect) {
            childrenUI.push(<div onClick={()=>onAddonDo("more")}
                                 key='sel' className="EField_add"
                                 style={{position: 'absolute', top: 0, right: 19}}>...</div>)
        }
        childrenUI.push(<div onClick={()=>onAddonDo("del")} key='x' className="EField_add" style={{position: 'absolute', top: 0, right: 0}}>×</div>);

        return <div className={className} {...uiProps}>
            {childrenUI}

        </div>;
    }
    const style = (ui.w && ui.h) ?
        {position: 'absolute', top: 0, right: 0, bottom: 0, left: 0}
        : undefined;

    return (<div {...uiProps}>
        <div className={className} style={style} title={value}>{value}</div>
    </div>);


};

EGridRowUI.isUi = true;
EGridRowUI.isRowUi = true;
EGridRowUI.flexToGroup = true;

export default EGridRowUI;

EGridRowUI.propTypes = {
    classNameUi: PropTypes.any,
    name: PropTypes.any,
    text: PropTypes.any
};
