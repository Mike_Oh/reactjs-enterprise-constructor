////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from "react";
import {useUi} from "../uiContext";
import ETextUI from "../piece/ETextUI";


const EGridScroll = (props) => {
    const {name, events} = props;

    const ui = useUi(props, EGridScroll, true);
    //const className='EGridRowUI';

    const keyS = name + 's';
    const keyB = name + 'b';

    //const propsS = {key: keyS, name: keyS, uiGroup: ui};


    const onClick=(key)=>{
        const e = {type: 'keydown', key:key};
        events.scroll(e);
    };

    const commonProps = {uiGroup: ui, className: 'EGrid_scroll_bt'};

    const propsB1 = {key: keyB+1, name: keyB+1, text:'⏮', onClick:()=>onClick("Home"), title:"to the first",...commonProps};
    const propsB2 = {key: keyB+2, name: keyB+2, text:'⏴', onClick:()=>onClick("PageUp"), title:"page back",...commonProps};
    const propsB3 = {key: keyB+3, name: keyB+3, text:'🗘', onClick:()=>onClick("Refresh"), title:"refresh",...commonProps};
    const propsB4 = {key: keyB+4, name: keyB+4, text:'⏵', onClick:()=>onClick("PageDown"), title:"page forward",...commonProps};
    const propsB5 = {key: keyB+5, name: keyB+5, text:'⏭', onClick:()=>onClick("End"), title:"to the last",...commonProps};

    commonProps.className = 'EGrid_scroll';
    commonProps.flexWidth = 'Y';
    const propsS1 = {key: keyS+1, name: keyS+1, text:'',...commonProps};

    // const uiProps = ui.uiProps(props);
    //
    // const addons = [
    //     {Type: ETextUI, name: 'pls', text: '⏷', className: 'EButton_add'},
    // ];

    return <>

        <ETextUI {...propsS1}/>
        <ETextUI {...propsB1}/>
        <ETextUI {...propsB2}/>
        <ETextUI {...propsB3}/>
        <ETextUI {...propsB4}/>
        <ETextUI {...propsB5}/>
    </>;

};

EGridScroll.isGroup = true;
EGridScroll.isHorizontal = true;


EGridScroll.propTypes = {

    // data: PropTypes.string,
    // name: PropTypes.string,
    // kind: PropTypes.oneOf(['INPUT', 'TEXTAREA', 'TEXT']).isRequired,
    // caption: PropTypes.string,
    // captionPosition: PropTypes.oneOf(['NONE', 'LEFT', 'TOP']),
    // flexWidth: PropTypes.oneOf(['Y', 'N', 'AUTO']),
    // flexHeight: PropTypes.oneOf(['Y', 'N', 'AUTO']),
    // width: PropTypes.number,
    // height: PropTypes.number,
    // maxWidth: PropTypes.number,
    // maxHeight: PropTypes.number,
    // visible: PropTypes.oneOf(['Y', 'N']),
    // uiProps: PropTypes.object,
    // onClick: PropTypes.func,
    // onChange: PropTypes.func,
};


EGridScroll.defaultProps = {
    flexWidth: 'Y',
    flexHeight: 'N',
    width: 1,
    height: 20,

};
export default EGridScroll;
