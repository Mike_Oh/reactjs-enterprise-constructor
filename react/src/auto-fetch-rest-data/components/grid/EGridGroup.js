////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import PropTypes from "prop-types";
import {useUi} from "../uiContext";
import EGridField from "./EGridField";
import {captionFromData} from "../../common/commonMetods";


const EGridGroup = (props) => {

    const {rowType, children, grouping, name, row} = props;
    const add = rowType==='h' ? '_h' : '_r';
    const ui = useUi(props, EGridGroup);

    if (grouping === 'TOGETHER') {
        const titles = [];
        let width = 0;
        let maxWidth = 0;
        let flexWidth = 'N';
        const data = [];
        React.Children.forEach(children, child => {
            if (child.props.flexWidth === 'Y') flexWidth = 'Y';
            width += child.props.width;
            maxWidth += child.props.maxWidth;
            titles.push(captionFromData(child.props.text, child.props.data));
            data.push(child.props.data)

        });

        const uiProps = {
            key: name + '_f', name: name + '_f', uiGroup: ui,
            text: titles.toString(),
            data: data,
            width: width,
            maxWidth: maxWidth,
            flexWidth: flexWidth,
            rowType: rowType,
            row:row,
        };

        return <EGridField {...uiProps}/>
    }

    const childrenUI = [];
    React.Children.forEach(children, child => {
        const {name, visible, ...otherProps} = child.props;
        if (!!otherProps.data&&ui.gridContext.dataCols.find(v=>v===otherProps.data)===undefined)
            ui.gridContext.dataCols.push(otherProps.data);
        if (!!otherProps.dataRef&&ui.gridContext.dataCols.find(v=>v===otherProps.dataRef)===undefined)
            ui.gridContext.dataCols.push(otherProps.dataRef);

        if (visible==='N') return;
        const H = child.type;
        const uiProps = {
            ...otherProps, key: name + add, name: name + add, uiGroup: ui, rowType: rowType,
            row:row,

        };

        childrenUI.push(<H {...uiProps}/>);

    });


    return childrenUI;


};

EGridGroup.isGroup = true;
EGridGroup.flexToGroup = true;

// EGridGroup.isVertical = true;

export default EGridGroup

EGridGroup.defSize = {
    minWidth: 50,
};

EGridGroup.propTypes = {
    name: PropTypes.string,
    text: PropTypes.string,
    grouping: PropTypes.oneOf(['HORIZONTAL', 'VERTICAL', 'TOGETHER']),
    visible: PropTypes.oneOf(['Y', 'N']),
};


EGridGroup.defaultProps = {
    name: '',
    visible: 'Y',
    grouping: 'VERTICAL',
};