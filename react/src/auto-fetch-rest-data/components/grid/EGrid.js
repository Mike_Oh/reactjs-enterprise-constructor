////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react'
import PropTypes from 'prop-types'
import {useUi} from "../uiContext";
import EGridGroup from "./EGridGroup";
import EForm from "../EForm";
import EGridVoid from "./EGridVoid";
import Api from "../../Api";
import EGridScroll from "./EGridScroll";
import {MetaContext} from "../../common/MainContext";
import {OpenForm, TimeStampToString} from "../../common/commonMetods";

class gridContext {
    constructor(props, ui) {
        this.ui = ui;
        this.currentRow = 0;
        this.firstGridRow = 0;
        this.activeCol = undefined;
        this.dataCols = [];
        this.dataFilter = props.dataFilter;
        this.dataOrderBy = props.dataOrderBy;
        this.dataSourceId = props.dataSourceId;
        if (props.main === "Y") ui.isMain = true;

        this.visibleRows = 0;
        this.isRest = (props.dataType === "HQL_REST");
        this.dataTable = {cols: [], rows: []};
        if (this.isRest) {
            this.dataName = props.dataSource || ui.form.params.addProps.entity.name;
        } else {
            this.dataName = props.dataSource;
            if (ui.form.formData.object.lists && ui.form.formData.object.lists[props.dataSource])
                this.dataTable = ui.form.formData.object.lists[this.dataName];
        }


    }

    value(row, col) {
        if (!this.dataTable) return '?';
        if (this.dataTable.rows.length <= row) return '??';

        if (col === "#") return (row + 1);

        const colN = this.dataTable.cols.findIndex(ch => ch === col || ch.data === col);

        if (colN === -1) return '???';
        const colType = this.dataTable.cols[colN];
        let value = this.dataTable.rows[row][colN];
        if (Array.isArray(value)) value = value[1];
        if (value === undefined) return null;
        if (colType.type === "B:datetime") value = TimeStampToString(value);
        return value;

    }

    set gridRow(row) {
        this.currentRow = this.firstGridRow + row;
    }

    get gridRow() {
        return this.currentRow - this.firstGridRow;
    };

    correctShift() {

        const totalDataRows = this.dataTable.rows.length;

        if (this.firstGridRow > totalDataRows - this.visibleRows && this.firstGridRow !== 0) {
            this.firstGridRow = Math.max(totalDataRows - this.visibleRows, 0);
            this.needUpdate = true;
        }

    }
    addRow(){

        const newRow = this.dataTable.cols.map(col=>col.type.split(":")[0] === "E"?[0, ""] : "");
        this.dataTable.rows.push(newRow);
        this.shiftRow(this.dataTable.rows.length-1);
        this.editMode = true;

    }
    deleteRow(){
        this.editMode = false;
        if (this.currentRow===undefined) return;

        this.dataTable.rows.splice(this.currentRow, 1);

        if (this.currentRow===this.dataTable.rows.length&&this.dataTable.rows.length) {
            this.currentRow-=1;
            this.shiftRow(this.currentRow);
        }
    }

    downRow(){

        this.editMode = false;
        const newRow = this.currentRow+1;
        if (newRow>=this.dataTable.rows.length) return;

        const current = this.dataTable.rows[this.currentRow];
        this.dataTable.rows[this.currentRow] = this.dataTable.rows[newRow];
        this.dataTable.rows[newRow] = current;
        this.shiftRow(newRow);
    }

    upRow(){

        this.editMode = false;
        const newRow = this.currentRow-1;
        if (newRow<0) return;

        const current = this.dataTable.rows[this.currentRow];
        this.dataTable.rows[this.currentRow] = this.dataTable.rows[newRow];
        this.dataTable.rows[newRow] = current;
        this.shiftRow(newRow);

    }

    shiftRow(newRow) {
        this.editMode = false;
        const dataTable = this.dataTable;
        const totalDataRows = dataTable.rows.length;
        if (dataTable.rows.length === 0) return;

        if (newRow < 0) newRow = 0;
        if (newRow > totalDataRows - 1) newRow = totalDataRows - 1;
        if (newRow === this.currentRow) {
            this.correctShift();
            return;
        }


        this.needUpdate = true;

        if (this.firstGridRow > newRow) this.firstGridRow = newRow;

        if (this.firstGridRow + this.visibleRows - 1 < newRow) this.firstGridRow = newRow - this.visibleRows + 1;

        this.currentRow = newRow;

        const limit = MetaContext.gridLimit;

        const firstIdInRows = dataTable.rows[0][0];
        const LastIdInRows = dataTable.rows[dataTable.rows.length - 1][0];
        if (newRow < limit / 2 && dataTable.first && dataTable.first[0][0] !== firstIdInRows)
            this.receiveData(-1, firstIdInRows);
        else if (dataTable.rows.length - newRow < limit / 2 && dataTable.last && dataTable.last[0][0] !== LastIdInRows)
            this.receiveData(1, LastIdInRows);


    }

    shiftWheel(e) {
        if (e.type !== 'wheel') return;
        let newRow = this.currentRow;
        const sign = e.deltaY < 0 ? -1 : 1;

        newRow += Math.ceil(e.deltaY / this.rowHeight * sign) * sign;

        this.shiftRow(newRow);
    }


    shiftRowKey(e) {

        const dataTable = this.dataTable;

        if (e.type !== 'keydown') return;
        if (e.ctrlKey || e.shiftKey || e.altKey) return;

        if (!dataTable.rows.length) return;
        const key = e.key;
        let newRow = this.currentRow;
        if (key === 'ArrowUp') newRow--;
        else if (key === 'ArrowDown') newRow++;
        else if (key === 'PageUp') newRow -= this.visibleRows;
        else if (key === 'PageDown') newRow += this.visibleRows;
        else if (key === 'Home') {
            if (dataTable.first) {
                dataTable.rows = [];
                addRows(dataTable.rows, dataTable.first, 0, 1);
            }
            newRow = 0;
            this.correctShift();
        } else if (key === 'End') {
            if (dataTable.last) {
                dataTable.rows = [];
                addRows(dataTable.rows, dataTable.last, 0, -1);
            }
            newRow = dataTable.rows.length - 1;
            this.correctShift();
        } else return;
        //todo cleanup rows if >2 limits from current
        this.shiftRow(newRow)

    }


    shiftCol(e) {

        if (e.type !== 'keydown') return;
        if (e.ctrlKey || e.shiftKey || e.altKey) return;

        if (!this.activeCol) return;
        const key = e.key;

        let shift;
        if (key === 'ArrowLeft') shift = -1;
        else if (key === 'ArrowRight') shift = 1;
        else return;
        const elements = this.ui.form.elements;
        const cols = elements.filter(child => child.gridContext === this && child.isRowUi).map(child => child.name);
        let index = cols.findIndex(child => child === this.activeCol);

        if (index === -1) return;
        index += shift;
        if (index === -1 || index === cols.length) return;
        this.activeCol = cols[index];
        this.needUpdate = true;

        const ui = elements.find(child => child.name === this.activeCol);

        const current = this.ui.ref.current;

        if (ui.x < current.scrollLeft || current.clientHeight <= ui.w) current.scrollTo(ui.x, 0);


        else if (ui.x + ui.w > current.scrollLeft + current.clientWidth) current.scrollTo(ui.x + ui.w - current.clientWidth, 0);


    }

    onPressEnter(e) {

        if (e.type !== 'keydown') return;
        if (e.ctrlKey || e.shiftKey || e.altKey) return;
        if (e.key !== 'Enter') return;
        this.doSelect()

    }

    doEdit(rowData) {
        try{
            if (rowData === undefined) rowData =  this.dataTable.rows[this.currentRow];
        }catch (e) {
            return;
        }




        if (this.isRest)
            OpenForm(this.dataName + '/' + rowData[0]);
        else {
            this.editMode = true;
            this.ui.form.updateForm()
        }
    };

    doNew() {

        if (this.isRest)
            OpenForm(this.dataName + '/0');
    };

    doDelete() {
        if (this.isRest)
            window.alert("Not supported.")
    };

    doSelect(rowData) {

        if (rowData === undefined) rowData = this.dataTable.rows[this.currentRow];

        if (this.ui.form.kind === "chooseForm" && this.ui.isMain)
            this.ui.form.choose([rowData[0], rowData[1]]);
        else this.doEdit(rowData);
    };
}

function addRows(to, from, limit, orderBy) {
    if (!from) return;

    for (let i = 0; i < from.length; i++) {
        if (limit && to.length >= limit) return;
        to.push(from[orderBy === 1 ? i : from.length - i - 1]);
    }


}

const useGridContext = (props, ui) => {

    ui.group.grid = ui;

    if (!ui.gridContext) {
        ui.gridContext = new gridContext(props, ui);
        // setTimeout(() => {
        //     grid.onReceiveData()
        // }, 3000)

    }

    if (!ui.isRest) {
        if (ui.form.formData.object && ui.form.formData.object.lists && ui.form.formData.object.lists[props.dataSource])
            ui.gridContext.dataTable = ui.form.formData.object.lists[props.dataSource];
    }

    const grid = ui.gridContext;

    const [state, setState] = React.useState(0);


    // if (!ui.isRest) {
    //     if (ui.form.formData.object.lists && ui.form.formData.object.lists[props.dataSource])
    //
    // }

    React.useLayoutEffect(() => {
        if (grid.isRest) {
            let current;
            if (ui.isMain && ui.form.params.addProps && ui.form.params.addProps.selectParams && ui.form.params.addProps.selectParams.current)
                current = ui.form.params.addProps.selectParams.current[0];
            grid.receiveData(0, current);
        }
    }, []);


    grid.onReceiveData = () => {
        // ui.gridContext.totalDataRows = ui.form.formDataSource[ui.gridContext.dataSource].length;
        //ui.gridContext = ui.form.formDataSource[ui.gridContext.dataSource].length;
        //setState(state + 1);
    };


    grid.receiveData = (type, currentRow) => {
        const colsArr = [grid.dataSourceId, ...grid.dataCols];
        const cols = colsArr.map(child => child);

        const then_receiveData = (r) => {
            if (r.data.isError) {
                console.log("err (Api.msg):  " + r.data.msg);
                console.log("           at:  " + r.data.place);
                return;
            }
            const tbl = r.data;
            //if (r.data.row)grid.dataTable.rows =r.data.row;
            const limit = MetaContext.gridLimit;

            const first = [], last = [], rows = [];


            if (type === -1) {
                addRows(rows, tbl.before, 0, -1);
                addRows(rows, grid.dataTable.rows, 2 * limit, 1);
                grid.dataTable.rows = rows;
                grid.currentRow += tbl.before.length;
                grid.firstGridRow += tbl.before.length;


            } else if (type === 1) {

                addRows(rows, grid.dataTable.rows, 0, 1);
                addRows(rows, tbl.after, 0, 1);
                grid.dataTable.rows = rows;
            } else if (type === 0) {
                addRows(first, tbl.first, limit, 1);
                addRows(first, tbl.before, limit, -1);
                addRows(first, tbl.current, limit, 1);
                addRows(first, tbl.after, limit, 1);
                addRows(first, tbl.last, limit, -1);

                addRows(last, tbl.last, limit, 1);
                addRows(last, tbl.after, limit, -1);
                addRows(last, tbl.current, limit, 1);
                addRows(last, tbl.before, limit, 1);
                addRows(last, tbl.first, limit, -1);

                addRows(rows, tbl.before, 0, -1);
                addRows(rows, tbl.current, 0, 1);
                addRows(rows, tbl.after, 0, 1);

                grid.dataTable.cols = tbl.cols;
                grid.dataTable.first = first;
                grid.dataTable.last = last;
                grid.dataTable.rows = rows;
                if (rows.length === 0) {
                    addRows(rows, last, 0, -1);
                    grid.shiftRow(rows.length - 1);
                } else {
                    const newRow = rows.findIndex(r => r[0] === currentRow);
                    if (newRow >= 0) {
                        grid.currentRow = newRow;
                        grid.firstGridRow = Math.max(newRow - Math.floor(grid.visibleRows / 2), 0);
                        //grid.shiftRow(newRow);
                    } else {
                        grid.shiftRow(rows.length - 1);
                    }
                }


            }
            grid.correctShift();
            setState(state + 1);
        };

        if (grid.receiveDataInProgress) return;
        grid.receiveDataInProgress = true;
        Api.grid(type, "" + cols, grid.dataName, currentRow, grid.dataOrderBy, grid.dataFilter)
            .then(r => {
                    then_receiveData(r);
                    grid.receiveDataInProgress = false;

                }
            ).catch((e) => {
                console.log(e);
                grid.receiveDataInProgress = false;
            }
        );


    };

    grid.onRefresh = (e) => {
        if (e.type !== 'keydown') return;
        if (e.key !== 'Refresh') return;
        grid.refresh();

    };

    grid.refresh = () => {

        //    return;
        const current = grid.dataTable.rows === undefined
        || grid.dataTable.rows.length === 0
        || isNaN(grid.currentRow)
        || grid.currentRow === undefined ? 0 : grid.dataTable.rows[grid.currentRow][0];
        grid.receiveDataInProgress = false;
        grid.receiveData(0, current);
        //console.log('123');

    };

    props.events.scroll = (e) => {
        ui.ref.current.focus();
        grid.onNavigate(e);
    };


    grid.onNavigate = (e) => {

        grid.onRefresh(e);
        grid.shiftRowKey(e);
        grid.shiftWheel(e);
        grid.shiftCol(e);
        grid.onPressEnter(e);

        if (!grid.needUpdate) return;
        grid.needUpdate = false;

        // if (grid.inUpdateState) return;
        //
        // grid.inUpdateState = true;
        // console.log(state);
        // setTimeout(() => {
        //     if (grid.inUpdateState) {
        //         grid.inUpdateState = false;
        setState(state + 1);
        // }
        // }, 3);


    };

    grid.onDoubleClick = (rowData, col) => {

        if (rowData === undefined) return;

        const params = {col: col, id: rowData[0], abort: false, dataName: grid.dataName};
        grid.ui.onHook('onDoubleClick', params);


        //OpenForm(this.params.dataName+'/'+this.params.id);

        if (!params.abort) grid.doSelect(rowData);

    };

    grid.onSelect = (row, col) => {

        grid.gridRow = row;
        grid.activeCol = col;
        setState(state + 1);
    };


    return grid;
};

const EGridContainer = (props) => {
    const form = React.useContext(EForm.context);

    //React.useContext(EForm.DataContext);// needs to use only in no-dynamic grid
    const ui = useUi(props, EGridContainer);
    ui.hooks = ui.getHooks(props, EGrid);
    if (props.main === "Y") ui.isMain = true;

    const grid = useGridContext(props, ui);

    React.useContext(EForm.DataContext);

    const {children, name} = props;

    const keyH = name + 'h';
    const keyR = name + 'r';
    const keyC = name + 'c';

    const propsH = {key: keyH, name: keyH, uiGroup: ui, rowType: 'h', grouping: "HORIZONTAL"};
    const propsR = {key: keyR, name: keyR, uiGroup: ui, rowType: 'b', grouping: "HORIZONTAL"};
    const propsC = {key: keyC, name: keyC, uiGroup: ui};


    const childrenUI = [<EGridGroup {...propsH}>{children}</EGridGroup>];

    const uiRow = form.elements.find(element => element.name === keyR);
    grid.rowHeight = !uiRow ? 0 : uiRow.h;

    const uiVoid = form.elements.find(element => element.name === keyC);
    let visibleRows = 0;

    const totalDataRows = grid.dataTable.rows.length;

    if (uiVoid) {

        visibleRows = Math.ceil(uiVoid.h / grid.rowHeight);
        grid.visibleRows = visibleRows;

        const maxVisibleRows = totalDataRows - grid.firstGridRow - 1;
        if (visibleRows > maxVisibleRows) {
            visibleRows = maxVisibleRows;
            grid.noShift = true;
        } else
            grid.noShift = false;

        const hVisibleRows = grid.rowHeight * visibleRows;
        uiVoid.w = uiRow.w;
        uiVoid.yy = uiVoid.y + hVisibleRows;
        uiVoid.hh = uiVoid.h - hVisibleRows;
    }

    for (let row = 0; row <= visibleRows; row++) {
        propsR.row = row;
        propsR.key = keyR + (grid.firstGridRow + row);
        childrenUI.push(<EGridGroup {...propsR}>{children}</EGridGroup>)
    }


    childrenUI.push(
        <EGridVoid {...propsC}/>
    );

    const uiProps = ui.divProps(props);
    uiProps.onKeyDown = grid.onNavigate;
    uiProps.onWheel = grid.onNavigate;
    uiProps.onFocus = ui.onFocus.bind(ui);
    uiProps.tabIndex = '0';

    return <div {...uiProps}>{childrenUI}</div>;


};

EGridContainer.isGroup = true;
EGridContainer.minWidthIgnoringChildren = 100;
EGridContainer.isGrid = true;
EGridContainer.isVertical = true;


const EGrid = (props) => {
    const {children, name, uiGroup, ...otherProps} = props;
    const ui = useUi(props, EGrid, true);
    otherProps.key = name + '_c';
    otherProps.name = name + '_c';
    otherProps.uiGroup = ui;
    const events = {};
    otherProps.events = events;

    const keyS = name + '_s';
    const propsS = {key: keyS, name: keyS, uiGroup: ui, events: events};

    return <>
        <EGridContainer {...otherProps}>{children}</EGridContainer>
        <EGridScroll {...propsS}/>
    </>
};


EGrid.isComponent = true;
EGrid.isVertical = true;


export default EGrid;


EGrid.propTypes = {

    dataType: PropTypes.oneOf(['OBJECT_FIELD', 'HQL_REST']),
    dataSource: PropTypes.any,
    dataFilter: PropTypes.string,
    dataOrderBy: PropTypes.string,
    main: PropTypes.oneOf(['Y', 'N']),
    name: PropTypes.string,
    caption: PropTypes.string,
    captionPosition: PropTypes.oneOf(['NONE', 'LEFT', 'TOP']),
    flexWidth: PropTypes.oneOf(['Y', 'N']),
    flexHeight: PropTypes.oneOf(['Y', 'N']),

    width: PropTypes.number,
    height: PropTypes.number,

    maxWidth: PropTypes.number,
    maxHeight: PropTypes.number,
    visible: PropTypes.oneOf(['Y', 'N']),
    onDoubleClick: PropTypes.func,
};


EGrid.defaultProps = {
    dataType: 'OBJECT_FIELD',
    dataSource: '',
    dataOrderBy: "",
    dataFilter: '',
    dataSourceId: 'id',
    name: '',
    main: 'N',
    captionPosition: 'LEFT',
    flexWidth: 'Y',
    flexHeight: 'Y',
    width: 0,
    height: 0,
    maxWidth: 0,
    maxHeight: 0,
    visible: 'Y',
    className: 'EGrid'

};
