////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import PropTypes from 'prop-types'
import React from 'react'
import {useUi} from '../uiContext'

const ECaptionUI = (props) => {
    const {text} = props;

    const ui = useUi(props, ECaptionUI);

    return (<label {...ui.uiProps(props)}>{text}</label>);

};

ECaptionUI.isUi = true;



ECaptionUI.propTypes = {
  text: PropTypes.any
};

ECaptionUI.defaultProps = {

};

export default ECaptionUI;
