////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import PropTypes from 'prop-types'
import React from "react";
import {useUi} from "../uiContext";
import {useData} from "../dataContext";
import {setTypeRestrictionForInput} from "../../common/typeControl";

const EInputUI = (props) => {
    //console.log("EInputUI");

    const ui = useUi(props, EInputUI, true);

    const value = useData(props);

    const uiProps = ui.uiProps(props);
    const current = ui.ref.current;
    if (current) current.value = value;
    setTypeRestrictionForInput(uiProps, ui.getDataType());

    uiProps.onKeyDown = (e) => ui.group.onKeyDown(e);
    uiProps.onFocus=ui.onFocus.bind(ui);
    return (<input {...uiProps}/>);

};

EInputUI.isUi = true;


EInputUI.propTypes = {
    value: PropTypes.any
};

EInputUI.defaultProps = {};
export default EInputUI;
