////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from "react"
import {useUi} from "../uiContext"
import {MetaContext} from "../../common/MainContext";


const ECoverUI = (props) => {
    const {children}=props;
    const ui = useUi(props, ECoverUI, true);

    const uiProps = ui.uiProps(props);

    const [state, setState] = React.useState({bodySize: {}});

    const updateState = () => {
        setState({...state});
    };

    React.useLayoutEffect(() => {
        if (!uiProps.ref.current) return;
        const rect = uiProps.ref.current.getBoundingClientRect();
        if (rect.width !== state.bodySize.width || rect.height !== state.bodySize.height) {
            state.bodySize = {width: rect.width, height: rect.height};
            updateState();
        }
    });
    return (<div {...uiProps}>
        <MetaContext.useContainerSize.Provider value={state.bodySize}>

        {children}
        </MetaContext.useContainerSize.Provider>
    </div>);

};





ECoverUI.isUi = true;
ECoverUI.isCover = true;

ECoverUI.propTypes = {
};

ECoverUI.defaultProps = {
};
export default ECoverUI;
