////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import PropTypes from 'prop-types'
import React from 'react'
import {useUi} from '../uiContext'
import {useData} from "../dataContext";

const ETextUI = (props) => {
    const {text, title} = props;


    const ui = useUi(props, ETextUI);
    const value = useData(props);
    const v = (!!value)?value:text;
    // const v = text;

    return (<div title={title} {...ui.uiProps(props)}>{v}</div>);

};

ETextUI.isUi = true;


ETextUI.propTypes = {
  text: PropTypes.any
};

ETextUI.defaultProps = {

};
export default ETextUI;
