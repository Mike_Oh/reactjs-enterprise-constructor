////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import PropTypes from 'prop-types'
import React from "react"
import {useUi} from "../uiContext"
import {useData} from "../dataContext"

const ETextAreaUI = (props) => {

    const ui = useUi(props, ETextAreaUI, true);

    const value = useData(props);

    const uiProps = ui.uiProps(props);

    const current = ui.ref.current;
    if (current) current.value = value;

    if (props.flexHeight==='Y'&&props.flexWidth==='Y') uiProps.style.resize = 'none';
    else if (props.flexHeight==='Y') uiProps.style.resize = 'horizontal';
    else if (props.flexWidth==='Y') uiProps.style.resize = 'vertical';

    return (<textarea {...uiProps}/>);

};
ETextAreaUI.isUi = true;
ETextAreaUI.isResizable = true;

ETextAreaUI.propTypes = {
  flexHeight: PropTypes.any,
  flexWidth: PropTypes.any
};

ETextAreaUI.defaultProps = {};
export default ETextAreaUI;
