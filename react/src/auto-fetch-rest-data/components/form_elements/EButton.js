////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import PropTypes from "prop-types";
import {addAddons, useUi} from "../uiContext";
import ETextUI from "../piece/ETextUI";

const EButton = (props) => {

    const {text, name, ...otherProps} = props;

    const ui = useUi(props, EButton);

    if (!otherProps.width&&otherProps.flexWidth!=='N') otherProps.width = EButton.defSize.minWidth;

    const nameUI = name + '_ui';
    const UiBase = ETextUI;



    const uiPropsBase = {
        ...otherProps, key: nameUI, name: nameUI, uiGroup: ui,
        text: text,
        className: 'EButton_ui'
    };

    const childrenUI = [];

    childrenUI.push(
        <UiBase {...uiPropsBase}/>)
    ;

    const addons = [
        // {Type: ETextUI, name: 'pls', text: '⏷', className: 'EButton_add'},
    ];

    addAddons(childrenUI, addons, ui);

    return childrenUI;
};

EButton.isComponent = true;
EButton.isHorizontal = true;

EButton.defSize = {
    minWidth: 10,
};

export default EButton;

EButton.propTypes = {

    name: PropTypes.string,
    text: PropTypes.string,
    caption: PropTypes.string,
    captionPosition: PropTypes.oneOf(['NONE', 'LEFT', 'TOP']),
    flexWidth: PropTypes.oneOf(['Y', 'N']),
    flexHeight: PropTypes.oneOf(['Y', 'N']),
    width: PropTypes.number,
    height: PropTypes.number,
    maxWidth: PropTypes.number,
    maxHeight: PropTypes.number,
    visible: PropTypes.oneOf(['Y', 'N']),
    uiProps: PropTypes.object,
    onClick: PropTypes.func,
};


EButton.defaultProps = {
    name: '',
    caption: '',
    text: '',
    captionPosition: 'LEFT',
    flexWidth: 'N',
    flexHeight: 'N',
    width: 0,//to-do add auto for 0 width
    height: 0, //to-do add auto for 0 height
    maxWidth: 0,
    maxHeight: 0,
    visible: 'Y',
};