////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import PropTypes from 'prop-types'
import React from 'react'
import {useUi} from "../uiContext";
import EWithCaption from "../containers/EWithCaption";
import EPagesList from "../containers/EPagesList";
import {captionFromData} from "../../common/commonMetods";

function usePages() {

    const [activePage, setActivePage] = React.useState(0);

    const onSelectPage = (selected) => {
        setActivePage(selected)
    };


    return {activePage: activePage, onSelectPage: onSelectPage};


}

const EGroup = (props) => {

    const {className, name, activePage, isPagesBody, pagesClassName, isPages} = props;

    const children = React.Children.toArray(props.children);
    const ui = useUi(props, EGroup);

    const pages = usePages(props, ui);
    let groupChildren;


    if (isPages) {

        const titles = children.map(child => ({caption: captionFromData(child.props.caption, child.props.dataSource), visible: child.props.visible}));

        const childProps_b = {
            key: name + '_b', name: name + '_b',
            uiGroup: ui, grouping: 'HORIZONTAL',
        };

        const childProps = {key: name + '_t1', name: name + '_t1', uiGroup: ui, grouping: 'PAGES'};
        if (!className) childProps.className = className;

        const pClassName = pagesClassName || 'EPagesList';

        childProps.className = pClassName + 'Body';


        groupChildren = <>
            <EPagesList {...childProps_b} titles={titles} activePage={pages.activePage}
                        onSelectPage={pages.onSelectPage} pagesClassName={pClassName}/>
            <EGroup {...childProps} activePage={pages.activePage} isPagesBody>
                {children}
            </EGroup>

        </>


    } else {
        groupChildren = children.map((child, index) => {
            const {caption, captionPosition, name, visible, className, data} = child.props;
            const childName = name + '_container';
            const normalCaption = captionFromData(caption, data);
            const groupProps = {
                key: childName, name: childName, uiGroup: ui,
                captionPresent: ['LEFT', 'TOP'].includes(captionPosition) && (!!normalCaption) && (!isPagesBody),
                grouping: captionPosition === 'TOP' ? 'VERTICAL' : 'HORIZONTAL',
                caption: normalCaption,
                visible: visible,
            };

            if (child.type.isComponent)groupProps.margin=true;
            else if (!!className)groupProps.margin=true;
            if (isPagesBody) groupProps.visible = activePage === index ? 'Y' : 'N';

            return <EWithCaption {...groupProps}>{child}</EWithCaption>

        });
    }

    if (!className) return groupChildren;
    return <div {...ui.divProps(props)}>{groupChildren}</div>;

};

EGroup.isGroup = true;

export default EGroup;

EGroup.propTypes = {

    //data:PropTypes.string,
    name: PropTypes.string,
    caption: PropTypes.string,
    grouping: PropTypes.oneOf(['HORIZONTAL', 'VERTICAL', 'PAGES', 'MENU-not yet']),
    captionPosition: PropTypes.oneOf(['NONE', 'LEFT', 'TOP']),
    flexWidth: PropTypes.oneOf(['Y', 'N']),
    flexHeight: PropTypes.oneOf(['Y', 'N']),
    width: PropTypes.number,
    height: PropTypes.number,
    maxWidth: PropTypes.number,
    maxHeight: PropTypes.number,
    visible: PropTypes.oneOf(['Y', 'N']),
    sameCaptionSizeIn1Column: PropTypes.oneOf(['Y', 'N']),
    className: PropTypes.string,
};


EGroup.defaultProps = {
    data: '',
    name: '',
    caption: '',
    captionPosition: 'LEFT',
    grouping: 'HORIZONTAL',
    flexWidth: 'Y',
    flexHeight: 'Y',
    width: 0,//to-do add auto for 0 width
    height: 0, //to-do add auto for 0 height
    maxWidth: 0,
    maxHeight: 0,
    visible: 'Y',
    sameCaptionSizeIn1Column: 'N',

};
