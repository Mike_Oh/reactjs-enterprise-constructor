////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import PropTypes from 'prop-types'
import React from 'react'
import {useUi} from "../uiContext";
import ECaptionUI from "../piece/ECaptionUI"


const ECaption = (props) => {

    const {isCaption, text, name, uiProps} = props;

    const ui = useUi(props, ECaption);
    const nameUI = name + '_ui';
    const UiBase = ECaptionUI;


    const uiPropsBase = {uiProps, key: nameUI, name: nameUI, uiGroup: ui,isCaption:isCaption,
        text: text,
        className: 'ECaption_ui'
    };

    const childrenUI = [];

    childrenUI.push(
        <UiBase {...uiPropsBase}/>);
    return childrenUI;


};
ECaption.isComponent = true;
ECaption.isHorizontal = true;



export default ECaption;

ECaption.propTypes = {
    text: PropTypes.string,
    caption: PropTypes.string,
    name: PropTypes.string,
    captionPosition: PropTypes.oneOf(['NONE', 'LEFT', 'TOP']),
};


ECaption.defaultProps = {
    name: '',
    text: '',
    caption: '',
    captionPosition: 'LEFT',
};