////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import PropTypes from "prop-types";
import EInputUI from "../piece/EInputUI";
import {addAddons, useUi} from "../uiContext";
import ETextUI from "../piece/ETextUI";
import ETextAreaUI from "../piece/ETextAreaUI";


const EField = (props) => {

    //console.log("EField");
    const {name, kind, ...otherProps} = props;

    const ui = useUi(props, EField, true);

    const nameUI = name + '_ui';
    const UiBase = kind === 'INPUT' ? EInputUI : kind === 'TEXTAREA' ? ETextAreaUI : ETextUI;

    if (otherProps.flexWidth === 'AUTO')
        if (kind === 'TEXTAREA')
            otherProps.flexWidth = 'Y';
        else otherProps.flexWidth = otherProps.width ? 'N' : 'Y';

    if (!otherProps.width) otherProps.width = EField.defSize.minWidth;

    if (otherProps.flexHeight === 'AUTO') otherProps.flexHeight = 'N';

    const uiPropsBase = {
        ...otherProps, key: nameUI, name: nameUI, uiGroup: ui,
        className: 'EField_ui', baseUI:true
    };

    const childrenUI = [];

    childrenUI.push(
        <UiBase {...uiPropsBase}/>)
    ;


    const addons = [];
    if (kind === 'INPUT') {
        const dataType = ui.getDataType();
        if (!!dataType&&dataType[0]==="E")
            addons.push({Type: ETextUI, name: 'more', text: '...', className: 'EField_add'});
        addons.push({Type: ETextUI, name: 'del', text: '×', className: 'EField_add'});
        //addons.push({Type: ETextUI, name: 'pls', text: '⌕', className: 'EField_add'});
    }else if (kind === 'TEXTAREA') {
        addons.push({Type: ETextUI, name: 'del', text: '×', className: 'EField_add'});
    }

    addAddons(childrenUI, addons, ui);

    return childrenUI;




};
EField.isComponent = true;
EField.isHorizontal = true;


export default EField

EField.defSize = {
    minWidth: 50,
};

EField.propTypes = {

    data: PropTypes.string,
    name: PropTypes.string,
    kind: PropTypes.oneOf(['INPUT', 'TEXTAREA', 'TEXT']).isRequired,
    caption: PropTypes.string,
    captionPosition: PropTypes.oneOf(['NONE', 'LEFT', 'TOP']),
    flexWidth: PropTypes.oneOf(['Y', 'N', 'AUTO']),
    flexHeight: PropTypes.oneOf(['Y', 'N', 'AUTO']),
    width: PropTypes.number,
    height: PropTypes.number,
    maxWidth: PropTypes.number,
    maxHeight: PropTypes.number,
    visible: PropTypes.oneOf(['Y', 'N']),
    uiProps: PropTypes.object,
    onClick: PropTypes.func,
    onChange: PropTypes.func,
};


EField.defaultProps = {
    data: '',
    name: '',
    kind: 'INPUT',
    captionPosition: 'LEFT',
    flexWidth: 'AUTO',
    flexHeight: 'AUTO',
    width: 0,//to-do add auto for 0 width
    height: 0, //to-do add auto for 0 height
    maxWidth: 0,
    maxHeight: 0,
    visible: 'Y',

};