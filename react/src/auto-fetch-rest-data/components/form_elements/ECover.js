////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import PropTypes from 'prop-types'
import React from "react"
import {useUi} from "../uiContext"
import ECoverUI from "../piece/ECoverUI";

const ECover = (props) => {

    const {children, name, ...otherProps} = props;
    const ui = useUi(props, ECover, true);

    const uiPropsBase = {
        ...otherProps, key: name + '_ui', name: name + '_ui', uiGroup: ui,
    };

    return <ECoverUI {...uiPropsBase}>
        {children}
    </ECoverUI>;


};
ECover.isComponent = true;
ECover.isVertical = true;
ECover.isCover = true;


ECover.propTypes = {
    name: PropTypes.any,
    caption: PropTypes.string,
    captionPosition: PropTypes.oneOf(['NONE', 'LEFT', 'TOP']),
    flexWidth: PropTypes.oneOf(['Y', 'N']),
    flexHeight: PropTypes.oneOf(['Y', 'N']),
    width: PropTypes.number,
    height: PropTypes.number,
    maxWidth: PropTypes.number,
    maxHeight: PropTypes.number,
    visible: PropTypes.oneOf(['Y', 'N']),

};

ECover.defaultProps = {
    name: '',
    caption: '',
    captionPosition: 'LEFT',
    flexWidth: 'N',
    flexHeight: 'N',
    width: 0,
    height: 0,
    maxWidth: 0,
    maxHeight: 0,
    visible: 'Y',
    className: 'ECover'
};
export default ECover;
