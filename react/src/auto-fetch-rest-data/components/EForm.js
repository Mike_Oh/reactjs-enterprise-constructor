////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import PropTypes from "prop-types";
import {useUiContext} from "./uiContext";
import {useDataContext} from "./dataContext";
import EGroup from "./form_elements/EGroup";
import EMemoContainer from "./containers/EMemoContainer";
import '../css/UI.css'
import {setHooks, setUserHooks} from "./uiHooks";
import {MetaContext} from "../common/MainContext";


/**
 * @typedef {{
 *     group:Element
 *     type:*,
 *     props:{
 *          data:string,
 *          name:string,
 *          caption: string,
 *          grouping:string,
 *          captionPosition:string,
 *          flexWidth : string,
 *          flexHeight: string,
 *          width:number,
 *          height: number,
 *          maxWidth: number,
 *          maxHeight: number,
 *          visible: string,
 *          sameCaptionSizeIn1Column: string
 *          }}
 *} Element_
 */

/**
 * @typedef {{
 *     initialisation: boolean
 *     lastElementKey: number
 *     elements:[]
 *     zNeedResize: boolean
 *     containerSize: {width: number, height: number}
 *     createElement: function(string<name>, Object<type>, Element<group>):Element
 *     createChildrenElements: function(children:*, parentName:string deleteOld:boolean)
 *     getChildrenElements: function(parentName:string|Object):Element[]
 * }} Form_
 *
 */

const useForm = (params) => {

    const [updateState, setUpdateState] = React.useState(0);

    const [form] = React.useState({
        isInitialisation: true,
        doUpdateData: false,
        doUpdateUI: false,
        uiContext:0,
        dataContext:0,
    });

    form.updateForm = (updateData=true, updateUI=true) => {
        if (updateData) form.doUpdateData = true;
        if (updateUI) form.doUpdateUI=true;
        setUpdateState(updateState + 1);
    };

    form.params = params;
    form.kind = params.addProps.kind;
    params.addProps.form = form;

    React.useEffect(() => {
        MetaContext.openForms.push(form);
        return () => {
            MetaContext.openForms.splice(MetaContext.openForms.findIndex(f=>f===form), 1);
        };
    }, []);


    return form;
};

const EForm = (props) => {
    //console.log("EForm");

    const {params, children, ...otherProps} = props;

    const form = useForm(params);

    setHooks(form);
    setUserHooks(form);

    useDataContext(props, form);
    useUiContext(props, form);

    const styleRootDiv = {
        position: 'absolute', left: 0, top: 0, right: 0, bottom: 0,
        overflowX: !form.scrollX ? 'hidden' : 'scroll',
        overflowY: !form.scrollY ? 'hidden' : 'scroll',

    };


    const rootUiProps = {
        className: 'FormRoot',
        style: styleRootDiv,
        onMouseUp: form.onMouseUp,
        ref: form.rootRef,
        // tabIndex:0,
        onKeyDown: form.onKeyDown,
    };

    if (!form.formData.ready)
        return <EForm.context.Provider value={form}>
            <div {...rootUiProps}>
            </div>
        </EForm.context.Provider>;


    return <EForm.context.Provider value={form}>
        <div {...rootUiProps}>
            <EForm.UiContext.Provider value={form.uiContext}>
                <EForm.DataContext.Provider value={form.dataContext}>
                    <EMemoContainer>
                        <EGroup uiGroup={undefined} {...otherProps}>{children}</EGroup>
                        {/*<button style={{position: 'absolute', right: 5, bottom: 5}} onClick={form.onClickTest}>test*/}
                        {/*</button>*/}
                    </EMemoContainer>
                </EForm.DataContext.Provider>
            </EForm.UiContext.Provider>
        </div>
    </EForm.context.Provider>;
};


EForm.context = React.createContext({});
EForm.UiContext = React.createContext({});
EForm.DataContext = React.createContext({});
EForm.isForm = true;

export default EForm;
//export default React.memo(EForm, () => true);


EForm.propTypes = {
    dataSource: PropTypes.any,
    dataType: PropTypes.oneOf(['OBJECT', 'HQL_REST']),
    name: PropTypes.string,
    caption: PropTypes.string,
    grouping: PropTypes.oneOf(['HORIZONTAL', 'VERTICAL']),
    // captionPosition: PropTypes.oneOf(['NONE', 'LEFT', 'TOP']),
    flexWidth: PropTypes.oneOf(['Y', 'N']),
    flexHeight: PropTypes.oneOf(['Y', 'N']),
    // width: PropTypes.number,
    // height: PropTypes.number,
    maxWidth: PropTypes.number,
    maxHeight: PropTypes.number,
    // visible: PropTypes.bool,
    sameCaptionSizeIn1Column: PropTypes.oneOf(['Y', 'N']),
    captionMaxWidth: PropTypes.number,
    className: PropTypes.string,
};


EForm.defaultProps = {
    dataSource: {},
    dataType: 'OBJECT',
    name: '',
    caption: '',
    captionPosition: 'NONE',
    grouping: 'HORIZONTAL',
    flexWidth: 'Y',
    flexHeight: 'Y',
    width: 0,//to-do add auto for 0 width
    height: 0, //to-do add auto for 0 height
    maxWidth: 0,
    maxHeight: 0,
    visible: 'Y',
    sameCaptionSizeIn1Column: 'Y',
    captionMaxWidth: 300,
    className: 'FormDefault',
    //className: '',

};


EForm.groupType = () => true;