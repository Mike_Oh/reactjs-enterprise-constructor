////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from "react";
import {MetaContext} from "../common/MainContext";
import {UiClass} from "./uiClass";
import EForm from "./EForm";


export const addAddons = (children, addons, ui) => {
    addons.forEach(bt => {
            const {Type, name, ...otherProps} = bt;

            const nameUI = ui.name + '_ui_add' + name;

            const onClickAdd = () => ui.onAddonDo(name);

            const uiPropsAdd = {...otherProps, key: nameUI, name: nameUI, uiGroup: ui, onClick: onClickAdd};

            children.push(<Type {...uiPropsAdd}/>);

        }
    );

};

export const useUi = (props, UiType) => {

    const form = React.useContext(EForm.context);
    React.useContext(EForm.UiContext);

    let ui = form.elements.find(element => element.name === props.name);
    if (!ui) ui = new UiClass(props, UiType, form);
    else ui.hidden = (props.visible === 'N') || (!!ui.group && ui.group.hidden);

    return ui;
};

export const useUiContext = (props, form) => {

    React.useLayoutEffect(() => {
        if (form.formData.ready) {
            form.elements.forEach(child => child.updateFp());
            form.updateForm();
        }
    }, [form.formData.ready]);

    if (!form.elements) form.elements = [];

    React.useEffect(() => {

        if (!form.activeElement) form.activeElement = form.mainElement();
        if (form.activeElement) form.activeElement.ref.current.focus();

    });


    useFormSize(form);

    if (!form.rootRef) form.rootRef = React.createRef();

    if (!form.formData.ready) return;

    if (form.isInitialisation) {

        delete form.isInitialisation;

        form.captionMaxWidth = props.captionMaxWidth;
        return;
    }


    if (!form.doUpdateUI) return;


    //0) set new uiContext to inform elements for updating
    form.uiContext++;
    form.doUpdateUI = false;


    const root = form.elements[0];

    //1) updateFp - get current size of all divs
    //form.elements.filter(child => true).forEach(child => child.updateFp());

    //2) setSameCaptionSize
    form.elements.filter(child => child.sameCaptionSizeIn1Column).forEach(child => child.setSameCaptionSize());

    //3) collect possible MinMax for each component and group
    root.collectMinMax();

    //4 calculate result size and add scroll(if needed)

    root.x = 0;
    root.y = 0;


    root.w = form.containerSize.width - root.x;
    root.h = form.containerSize.height - root.y;
    //MetaContext.showMessage('root.w='+root.w+'; root.h='+root.h);

    form.scrollY = (root.h < root.minH);
    if (form.scrollY) root.w -= MetaContext.scrollSizeY;

    form.scrollX = (root.w < root.minW);
    if (form.scrollX) {
        root.h -= MetaContext.scrollSizeX;
        form.scrollY = (root.h < root.minH);
        if (form.scrollY) root.w -= MetaContext.scrollSizeY;
    }

    //5
    root.setChildrenSize();

    //6
    root.setChildrenXY();


};

const useFormSize = (form) => {

    const containerSizeIn = React.useContext(MetaContext.useContainerSize);

    React.useLayoutEffect(() => {
        if (!containerSizeIn) return;
        if (containerSizeIn === form.containerSizeIn) return;
        form.containerSizeIn = containerSizeIn;

        const newSize = form.rootRef.current.getBoundingClientRect();
        const oldSize = form.containerSize;
        form.containerSize = newSize;

        if (!oldSize || oldSize.width !== newSize.width || oldSize.height !== newSize.height)
            form.doUpdateUI = true;

        form.updateForm();
    }, [containerSizeIn]);

};