////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import EForm from "../components/EForm";
import ECaption from "../components/form_elements/ECaption";
import React from "react";
import AutoObjectForm from "./AutoForms/AutoObjectForm";
import AutoForm from "./AutoForms/AutoForm";
import {camelCaseToNormal} from "./commonMetods";

export default class MetadataClass {
    entities = {};
    ui_home;
    ui_error;
    ui_showUnregisteredIn = "";
    ui = [];

    _ready = false;

    ready = () => this._ready;
    register = (restMetadata) => {

        //try {
        restMetadata.tables.forEach(t => {
            let entity = this.entities[t.name];
            if (!entity) {
                entity = {name: t.name, isTable: true, auto: true};
                this.entities[t.name] = entity;

                if (this.ui_showUnregisteredIn) {
                    let topic = this.ui.find(t => t.description === this.ui_showUnregisteredIn);

                    if (!topic) {
                        topic = {description: this.ui_showUnregisteredIn, list: []};
                        this.ui.push(topic);
                    }
                    topic.list.push({link: entity});
                }
            }
            entity.rest = t;

        });
        // }
        // catch(error) {
        //     console.log(error);
        // }

        this._ready = true;
    };

    getFormDescription = (entity, kind, id) => {
        if (kind === 'objectForm') return "" + (entity.description1 || entity.description || camelCaseToNormal(entity.name)) + ":" +
            (id === "0" ? "new" : id);
        return (kind === 'chooseForm' ? "←" : "") + (entity.description || camelCaseToNormal(entity.name));
    };

    generateForm(entity, kind) {
        if (!entity.isTable)
            return <EForm name={'Form'} grouping={"VERTICAL"}>
                <ECaption text={"this form not ready yet"}/>
            </EForm>;

        if (kind === "objectForm") {
            return AutoObjectForm(entity.rest, kind);
        } else {
            return AutoForm(entity.rest, kind);
        }


    }

}


