////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import {MetaContext} from "./MainContext";

export function camelCaseToNormal( myStr ) {
    if (myStr===undefined||myStr==="")return  myStr;
    if (myStr[myStr.length-1]===".")myStr = myStr.substr(0, myStr.length - 1);

    let rez =  myStr.replace( /([a-z])([A-Z])/g, '$1 $2' ).toLowerCase();
    return rez.substr(0,1).toUpperCase() + rez.slice(1);
}

export function captionFromData(caption, data) {
    return caption===undefined?camelCaseToNormal(data):caption;
}

export function TimeStampToString(TimeStamp) {
//    (new Date()).getTimezoneOffset();
    return TimeStamp;
    let v =new Date(TimeStamp);
    //let kjj =v.getTimezoneOffset();
    //let v2 =new Date(TimeStamp+(new Dat()).getTimezoneOffset()*1000*60*60);
   return v.toISOString().substr(0,16);

    return v.toLocaleString();
}


export function fillProps(to, from, what) {
    what.split(',').forEach(key => to[key] = from[key]);
}

export function fillPresentProps(to, from, what) {
    what.split(',').forEach(key => {
        if (!!from[key]) to[key] = from[key];
    });
}

export function zeroProps(to, what) {
    what.split(',').forEach(key => to[key] = 0);
}

export function OpenForm(formName, search) {
    const history = MetaContext.routerHistory;

    history.push("/" + formName + (search ? "?" + search : ""));
}

export function OpenSelectForm(fieldName) {
    const history = MetaContext.routerHistory;

    history.push(history.location.pathname + history.location.search + "<" + fieldName);
}

export function convertForSave(obj) {
    //return obj;

    // const rez = {attributes:{}, lists:{}};
    // const atr = obj.attributes;
    const rez = {};
    const atr = obj.attributes;

    Object.keys(atr).forEach(key => rez[key] = Array.isArray(atr[key]) ? parseInt(atr[key][0]) : atr[key]);

    Object.keys(obj.lists).forEach(listN => {
            const cols = obj.lists[listN].cols;
            rez[listN] = obj.lists[listN].rows.map(row => {
                    const rezRow = {};
                    cols.forEach((c, i) =>
                        rezRow[c.data] = Array.isArray(row[i]) ? parseInt(row[i][0]) : row[i]
                    );
                    return rezRow;
                }
            );

        }
    );
    return rez;

}
