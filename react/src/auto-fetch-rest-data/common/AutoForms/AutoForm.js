////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from "react";
import EForm from "../../components/EForm";
import EGroup from "../../components/form_elements/EGroup";
import EButton from "../../components/form_elements/EButton";
import EGridField from "../../components/grid/EGridField";
import EGrid from "../../components/grid/EGrid";


const AutoForm = (mdTable, kind) => {

    function doSelect() {
        this.mainElement.doSelect();
    }

    function doEdit() {
        this.mainElement.doEdit();
    }

    function doNew() {
        this.mainElement.doNew();
    }

    function doDelete() {
        this.mainElement.doDelete();
    }

    const jsxFields = mdTable.attributes
        .filter(f => f.data !== "id" && f.data !== "ref")
        .map((f, i) =>
            <EGridField key={i}
                        data={f.data + (f.type.substr(0, 1) !== "B" ? "." : "")}
            />);
    const buttons = [];


    if (kind === "chooseForm") buttons.push(<EButton key={0} text={"Select"} onClick={doSelect}/>);
    buttons.push(<EButton key={1} text={"Edit"} onClick={doEdit}/>);
    buttons.push(<EButton key={2} text={"New"} onClick={doNew}/>);
    buttons.push(<EButton key={4} text={"Delete"} onClick={doDelete}/>);

    return <EForm name={'Form'} grouping={"VERTICAL"}>
        <EGroup grouping={"HORIZONTAL"}>
            {buttons}
        </EGroup>
        <EGrid name={"mainGrid"} dataType={"HQL_REST"} main={"Y"}>
            {jsxFields}
        </EGrid>
    </EForm>

};


export default AutoForm;
