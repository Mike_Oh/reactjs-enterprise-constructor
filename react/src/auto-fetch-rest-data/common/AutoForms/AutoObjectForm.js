////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from "react";
import EField from "../../components/form_elements/EField";
import EForm from "../../components/EForm";
import EGroup from "../../components/form_elements/EGroup";
import EButton from "../../components/form_elements/EButton";
import {MetaContext} from "../MainContext";
import EGrid from "../../components/grid/EGrid";
import EGridField from "../../components/grid/EGridField";
import {camelCaseToNormal} from "../commonMetods";


const AutoObjectForm = (mdTable) => {

    function save() {

        this.save((data) => window.alert("Saved: " + data.id), (err) => {
            MetaContext.showMessage(err);
            window.alert("Error!")
        }, true);
    }

    function saveAndClose(p) {

        this.save(() => this.close(), (err) => {
            MetaContext.showMessage(err);
            window.alert("Error!")
        });
    }

    function close(p) {
        this.close();
    }


    let jsxA = mdTable.attributes
        .filter(f => f.data !== "id" && f.data !== "ref")
        .map((f, i) => <EField key={i} data={f.data}/>);
    const ll = mdTable.lists;
    let jsxT;
    if (mdTable.lists.length) {

        jsxT = <EGroup grouping={"PAGES"}>
            {
                mdTable.lists.map((list,key) => {
                    function addRow() {
                        const el = this.elements.find(el=>el.name===list.name);
                        el.grid.gridContext.addRow();
                    }

                    function deleteRow() {
                        const el = this.elements.find(el=>el.name===list.name);
                        el.grid.gridContext.deleteRow();
                    }

                    function upRow() {
                        const el = this.elements.find(el=>el.name===list.name);
                        el.grid.gridContext.upRow();
                    }

                    function downRow() {
                        const el = this.elements.find(el=>el.name===list.name);
                        el.grid.gridContext.downRow();
                    }

                    return <EGroup key={key} caption={camelCaseToNormal(list.name)} grouping={"VERTICAL"}>
                            <EGroup grouping={"HORIZONTAL"}>
                                <EButton text={"Add"} onClick={addRow}/>
                                <EButton text={"Delete"} onClick={deleteRow}/>
                                <EButton text={"↑"} onClick={upRow}/>
                                <EButton text={"↓"} onClick={downRow}/>
                            </EGroup>
                            <EGrid name={list.name}  key={list.name} dataSource={list.name}>
                                <EGridField text={'#'} data={"#"} flexWidth={"N"} width={30}/>
                                {

                                    list.attributes
                                        .filter(f => f.data !== "id").map((f, i) =>
                                        <EGridField key={i} data={f.data}/>)
                                }


                            </EGrid>
                        </EGroup>;
                    }
                )
            }
        </EGroup>;

        return <EForm name={'Form'} grouping={"VERTICAL"} dataType={"HQL_REST"}>
            <EGroup grouping={"HORIZONTAL"}>
                <EButton text={"Ok"} onClick={saveAndClose}/>
                <EButton text={"Save"} onClick={save}/>
            </EGroup>
            {jsxA}
            {jsxT}
        </EForm>;
    }

    return <EForm name={'Form'} grouping={"VERTICAL"} dataType={"HQL_REST"}>
        <EGroup grouping={"HORIZONTAL"}>
            <EButton text={"Ok"} onClick={saveAndClose}/>
            <EButton text={"Save"} onClick={save}/>
        </EGroup>
        {jsxA}
    </EForm>;


};


export default AutoObjectForm;
