////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import AppMetaData from "../../AppMetaData";


const AppMainContext = {

    RestUrl : `${document.location.protocol}//${document.location.hostname}:${9090}/`,

    useActiveForm: React.createContext(null),
    useContainerSize: React.createContext({width: 0, height: 0}),

    minHForFlexible: 30,//18
    gridLimit: 200,

    elementMarginW: 10,
    elementMarginH: 10,
    scrollSizeY: 15,
    scrollSizeX: 15,

    topicWidth: 200,
    headerHeight: 35,
    msgWindowSize: 200,

    //debug: {printLogInGroupContainer: false},
    msgArray:[],
    openForms:[],
    showMessage:null,

};

export const MetaData = AppMetaData;

export const MetaContext = AppMainContext;