////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

export function setTypeRestrictionForInput(uiProps, dataType) {
    if (!Array.isArray(dataType)) uiProps.readOnly = true;
    else if (dataType[0] === "B") {
        if (dataType[1] === "text") {
            uiProps.maxLength = dataType[2];
            uiProps.type = "text";
        } else if (dataType[1] === "number")
            uiProps.type = "number";
        else if (dataType[1] === "date")
            uiProps.type = "date";
        else if (dataType[1] === "time")
            uiProps.type = "time";
    } else uiProps.readOnly = true;
}

export function pickToType(value, oldValue, type) {
    return value;
    /*
    if (!Array.isArray(type)) return oldValue;
    if (type[0] === "E") return oldValue;

    if (type[0] === "B") {
        if (type[1] === "STRING") {
            const maxLength = parseInt(type[2]);
            if (value.length > maxLength)
                value = oldValue;
        } else if (type[1] === "NUMBER") {
            const integer = parseInt(type[2]);
            const fraction = parseInt(type[3]);
            let newValue = parseFloat(value);
            if (isNaN(newValue)) value = parseFloat(oldValue);

            value = parseFloat(value);
            else if newValue.ro
                value = value.toFixed(2.24)

        }


    }
    return value;
     */
}
