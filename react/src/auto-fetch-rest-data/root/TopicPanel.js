////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from "react";
import {Link} from "react-router-dom";
import {MetaData} from "../common/MainContext";
import {camelCaseToNormal} from "../common/commonMetods";


const TopicPanel = (props) => {
    const {activeTopic} = props;

    const jsx = MetaData.ui[activeTopic].list.map((el,i) => (
        <div key={i}>
            <Link to={"/"+el.link.name} className='tabCaption1' >
            {el.description||el.link.description||camelCaseToNormal(el.link.name)}
            </Link>
        </div> ));

    return (
        <div className={'MainBorder'}>
            {jsx}
        </div>
    )
};

export default TopicPanel;
