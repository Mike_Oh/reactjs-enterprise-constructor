////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import PropTypes from 'prop-types'
import React from 'react';
import '../css/EBody.css';
import {MetaContext, MetaData} from "../common/MainContext";

const EMainTabs = (props) => {

    //pages= {caption, key}
    const {body, height, className} = props;
    const [stateUpdate, setStateUpdate] = React.useState(0);
    if (!body.space) body.space = {ref: React.createRef()};


    React.useLayoutEffect(() => {

        let x = 0;
        let needUpdate = false;
        body.pages.forEach((page) => {

                const {offsetHeight, offsetWidth} = page.tab.ref.current;

                if (page.tab.left !== x || page.tab.top !== height - offsetHeight) {
                    needUpdate = true;
                    page.tab.left = x;
                    page.tab.top = height - offsetHeight;
                }
                x = x + offsetWidth;
            }
        );

        const page = body.space;
        const {offsetHeight} = page.ref.current;
        if (page.left !== x || page.top !== height - offsetHeight) {
            needUpdate = true;
            page.left = x;
            page.top = height - offsetHeight;
        }


        if (needUpdate)
            setStateUpdate(stateUpdate + 1)

    });


    const children = [];

    const styleBtnClose = {
        position: 'absolute',
        top: (0) + 'px',
        right: (0) + 'px',
    };

    const styleBtnAbout = {
        position: 'absolute',
        top: (0) + 'px',
        left: (0) + 'px',
    };

    const onClickTab = (path) => {
        const history = MetaContext.routerHistory;
        history.push(path);
    };

    const uiPropsSpace = {
        key: 0,
        className: className + 'Space',
        ref: body.space.ref,
        style: {left: body.space.left || 0, top: body.space.top || 0, right: 0},

    };
    children.push(<div {...uiPropsSpace}/>);

    body.pages.forEach((page) => {
            page.updateTabs = () => setStateUpdate(stateUpdate + 1);

            if (!page.tab) page.tab = {ref: React.createRef()};

            const uiPropsBase1 = {
                className: className + (body.activePage === page ? 'Active' : ''),
                ref: page.tab.ref,
                key: page.key,
                style: {left: page.tab.left || 0, top: page.tab.top || 0},

            };

            const uiPropsBase2 = {
                className: className + 'Text',
            };

            const jsx = [
                <div key ={0} {...uiPropsBase2} onClick={() => onClickTab(page.pathname)}>
                    {MetaData.getFormDescription(page.entity, page.kind, page.id)}
                </div>
            ];
            const isHome = page.pathname === "/";
            if (!isHome || body.pages.length > 1) jsx.push(
                <div key ={1} className='BtnClosePanel' style={styleBtnClose} onClick={page.close}
                     title={'Close form'}>×
                </div>
            );
            if (!isHome) jsx.push(
                <div key ={2} className='BtnClosePanel' style={styleBtnAbout} onClick={page.about}
                     title={'About this form...'}>*
                </div>
            );

            children.push(
                <div {...uiPropsBase1}> {jsx} </div>
            )
        }
    );


// const uiPropsBase = {
//     width: 1, flexWidth: 'Y', uiGroup: ui,
//     key: name + '_c', name: name + '_c', text: '\xa0',
//     className: 'BPagesListSpace'
// };
//
//
// children.push(<UiBase {...uiPropsBase}/>);

    return <>{children}</>;


};


export default EMainTabs;

EMainTabs.propTypes = {};


EMainTabs.defaultProps = {};

EMainTabs.propTypes = {
    className: PropTypes.any.isRequired,
    body: PropTypes.any.isRequired,
    // width: PropTypes.any.isRequired
};