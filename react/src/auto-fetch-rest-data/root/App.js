////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react';

import MainWindow from "./MainWindow";

import {BrowserRouter as Router, Route} from 'react-router-dom'
import {MetaContext} from "../common/MainContext";
import AuthorizationForm from "../../forms/system/AuthorizationForm";

function App() {
    const Context = (props) => {
        const activeForm = {...props.history.location};
        MetaContext.routerHistory = props.history;
        return <MetaContext.useActiveForm.Provider value={activeForm}>
            <MainWindow/>
        </MetaContext.useActiveForm.Provider>
    };


    const needAuthorization = AuthorizationForm.needAuthorization();

    return (

        <Router>
            <Route component={needAuthorization ? AuthorizationForm : Context}/>
        </Router>

    );

}


export default App;
