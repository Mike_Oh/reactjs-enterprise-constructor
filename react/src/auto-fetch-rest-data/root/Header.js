////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import {MetaData} from "../common/MainContext";
import '../css/EBody.css';


const Header = (props) => {
    const {activeTopic, setActiveTopic} = props;

    const jsx = MetaData.ui.map((el,i) => (
        <div key={i}
             className={"HeaderTab"+(i===activeTopic?" HeaderTabActive":"")}
             onClick={()=>setActiveTopic(i)}
             title={el.title}
        >
                {el.description}
        </div> ));

    return (
        <div className={'MainBorder'}>
            {jsx}
        </div>
    );
};

export default Header;
