////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import '../css/EBody.css';
import EMainTabs from "./EMainTabs";
import EFormStarter from "../components/EFormStarter";
import EMemoContainer from "../components/containers/EMemoContainer";
import {MetaContext, MetaData} from "../common/MainContext";

function useBody() {
    const [state, setState] = React.useState(1);
    const [body] = React.useState({counter: 1});
    if (!body.pages) body.pages = [];

    body.update = () => {
        setState(state + 1)
    };


    return body;

}

function useActiveForm(body) {
    const {pages} = body;
    const newLink = React.useContext(MetaContext.useActiveForm);
    const newPathName = newLink.pathname;


    let formParams = pages.find(page => page.pathname === newPathName);

    if (!formParams) {
        formParams = {pathname: newPathName, key: "" + (body.counter++)};
        let pathParts = newPathName.split("<");
        if (pathParts.length > 1) {
            const ownerField = pathParts.pop();
            const pageOwner = pages.find(page => page.pathname === ""+pathParts);
            if (pageOwner) {
                formParams.kind = 'chooseForm';
                //formParams.ownerField = ownerField;
                formParams.selectParams = pageOwner.form.getSelectParams(ownerField);
                formParams.selectParams.owner = pageOwner;
                formParams.entity = MetaData.entities[formParams.selectParams.type[1]];
            }
        } else {
            pathParts = newPathName.split("/");

            if (newPathName === "/") {
                formParams.kind = 'form';
                formParams.entity = MetaData.ui_home.link;
            } else if (pathParts.length === 2) {
                formParams.kind = 'form';
                formParams.entity = MetaData.entities[pathParts[1]];
            } else {
                formParams.kind = 'objectForm';
                formParams.entity = MetaData.entities[pathParts[1]];
                formParams.id = pathParts[2];
            }
        }
        if (!formParams.kind) {
            formParams.kind = 'form';
            formParams.entity = MetaData.ui_error.link;

        }

        pages.push(formParams);
    }

    body.onDeletePage = (page) => {
        const pIndex = pages.findIndex(p => page === p);
        pages.splice(pIndex, 1);

        if (page === formParams) {
            let newPage;
            if (pages.length > pIndex) newPage = pages[pIndex];
            else if (pIndex > 0) newPage = pages[pIndex - 1];
            else newPage = {pathname: ""};
            const history = MetaContext.routerHistory;
            history.push(newPage.pathname);

        } else {
            body.update();
        }
    };

    formParams.close = ()=>{
        body.onDeletePage(formParams);
    };

    formParams.about = ()=>{
        MetaData.showAboutForms(formParams);

    };

    return formParams;
}

const EBody = (props) => {

    const {bodySize} = props;
    const body = useBody();
    const {pages} = body;
    const activePage = useActiveForm(body);
    body.activePage = activePage;

    const jsx = pages.map(page => {
        const component = page.entity[page.kind];

        return <div key={page.key} style={{position: 'absolute', left: 0, bottom: 0, right: 0, top: '30px'}}
                    className={'EMainPagesBody'} hidden={activePage !== page}>
            <MetaContext.useContainerSize.Provider value={activePage === page ? bodySize : null}>
                <EMemoContainer>
                    <H pathname={page.pathname} search={page.search} component={component} addProps={page}/>
                </EMemoContainer>
            </MetaContext.useContainerSize.Provider>
        </div>
    });

    return <>
        <EMainTabs className={'EMainPages'} body={body} height={30}/>
        {jsx}
    </>;
};

const H = (props) => {

    const {pathname, search, addProps, component} = props;


    const formDataSource = {
        pathname: pathname,
        search: search,
        addProps: addProps,
    };

    return <EFormStarter
        component={component}
        params={formDataSource}
    />
};


export default EBody;

