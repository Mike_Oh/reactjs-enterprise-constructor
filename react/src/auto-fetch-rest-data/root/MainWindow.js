////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import {MetaContext, MetaData} from "../common/MainContext";
import Header from "./Header";
import TopicPanel from "./TopicPanel";
import MessageWindow from "./MessageWindow";
import EBody from "./EBody";
import Api from "../Api";


function MainWindow() {

    const [state, setState] = React.useState({ref: React.createRef(), bodySize: null, msq:"connecting"});
    const [activeTopic, setActiveTopic] = React.useState(0);

    const headerHeight = useHeaderHeight();
    const topicWidth = useTopicWidth();


    const msg = useMsgWindow();


    const updateState = () => {
        setState({...state});
    };

    if (!MetaData.ready())
        Api.getMD()
            .then(e => {
                MetaData.register(e.data);
                setState({...state});
            })
            .catch(e => {
                console.log(e);
                state.msq = "error: "+JSON.stringify(e);
                // window.alert("Can't connect to rest-server!   ----   ")
                updateState();
            });


    React.useLayoutEffect(() => {
        if (!MetaData.ready()) return;
        const rect = state.ref.current.getBoundingClientRect();
        if (!state.bodySize || rect.width !== state.bodySize.width || rect.height !== state.bodySize.height) {
            state.bodySize = {width: rect.width, height: rect.height};
            updateState();
        }
    });

    React.useEffect(() => {
        const handleResize = () => {
            updateState()
        };
        window.addEventListener('resize', handleResize);
        return () => {
            window.removeEventListener('resize', handleResize)
        }
    }, []);

    MetaContext.showMessage = (newMsg) => {
        if (newMsg === false)
            MetaContext.msgArray.splice(0);
        else
            MetaContext.msgArray.push(newMsg);
        updateState();
    };

    if (!MetaData.ready()) return <> {state.msq}</>;
    return (
        <>

            <div style={{
                position: 'absolute', left: 2 + 'px', top: 2 + 'px', right: 2 + 'px', height: headerHeight - 2 + 'px'
            }}>
                <Header activeTopic={activeTopic} setActiveTopic={setActiveTopic}/>
            </div>

            <div style={{
                position: 'absolute', left: 2 + 'px', top: headerHeight + 2 + 'px',
                width: topicWidth - 2 + 'px', bottom: msg.height + 2 + 'px'
            }}>
                <TopicPanel activeTopic={activeTopic}/>
            </div>

            {msg.visible ?
                <div style={{
                    position: 'absolute',
                    left: 2 + 'px',
                    bottom: 2 + 'px',
                    height: msg.height - 2 + 'px',
                    right: 2 + 'px'
                }}>
                    <MessageWindow/>
                </div> : null}

            <div ref={state.ref} className={'EBody'} style={{
                position: 'absolute', right: 2 + 'px', top: headerHeight + 2 + 'px',
                left: topicWidth + 2 + 'px', bottom: msg.height + 2 + 'px'
            }}>
                <EBody bodySize={state.bodySize}/>
            </div>

        </>
    );
}

const useMsgWindow = () => {
    //React.useContext(MetaContext.useContainerSize);
    // const [msg, setMsg] = React.useState({visible: false, height: 150});


    const visible = !!MetaContext.msgArray.length;
    return ({visible: visible, height: visible ? 150 : 0});
    //return ({visible: msg.visible, height: msg.visible ? msg.height : 0});
};


const useTopicWidth = () => MetaContext.topicWidth;
const useHeaderHeight = () => MetaContext.headerHeight;

export default MainWindow;
