////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import React from 'react';
import {MetaContext} from "../common/MainContext";


const MessageWindow = (props) => {
    const {hidden} = props;
    // const containerSize = useContainerSize();
    const [status] = React.useState({});
    if (!status.ref)status.ref = React.createRef();


    React.useEffect(() => {
        return () => {
            status.ref.current.scrollBy(0, 10000);
        };
    });
    const onClose = () => {
        MetaContext.showMessage(false);
    };

    const style = {
        overflowY: 'scroll',
        overflowX: 'auto'
    };
    const styleBtn = {
        position: 'absolute',
        top: (0) + 'px',
        right: (18) + 'px',
    };

    return (<>
            <div hidden={hidden} className='MainBorder' style={style} ref={status.ref}>
                {MetaContext.msgArray.map((m, i) => <div key={i}>{m}</div>)}
            </div>
            <div hidden={hidden} className='BtnClosePanel' style={styleBtn} onClick={onClose} title={'Close message windows'}>×</div>
        </>
    )
};


export default MessageWindow;
