////////////////////////////////////////////////////////////////////////////////
//      Copyright (c) 2019.  Mikhail Okhin <mike.okhin+copyright@gmail.com>    /
//                           All rights reserved.                              /
////////////////////////////////////////////////////////////////////////////////

import axoios from "axios";
import {MetaContext} from "./common/MainContext";


class Api {

    get path(){
        return MetaContext.RestUrl;
    }

    test(){
        return axoios.get(this.path+'test');
    }

    getMD(){
        const rest = this.path+'metadata';
        console.log("===getMD-<--: "+rest);
        return axoios.get(rest);
    }

    grid(type, fields, source, currentId=0, orderBy="", where=""){
        const limit = MetaContext.gridLimit;
        const rest = this.path+"grid" +
            "?fields="+fields+
            "&source="+source+
            "&currentId="+currentId+
            "&orderBy="+orderBy+
            "&where="+where+
            "&limit="+limit+
            "&type="+type;
        console.log("===grid-<--: "+rest);
        return axoios.get(rest);
    }

    record(tableName,id){
        const rest = this.path+'record/'+tableName+'/'+id;
        console.log("===record-<--: "+rest);
        return axoios.get(rest);
    }

    save(tableName, fields, id){
        return axoios.post(this.path+'record/'+tableName+"/"+id, fields);
    }

}

export default new Api();